<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<section id="content" <?php post_class();?>>
	<div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center">
      <div class="believe uk-text-center">
        <h3><?php the_title();?></h3>
        <?php if(has_excerpt()):
              the_excerpt();
              endif; ?>
      </div>
    </div>
  </div>
  <div class="uk-block">
    <div class="uk-container uk-container-center">
      <div class="uk-block-birds"></div>
      <div class="main-content">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <?php if(get_field('parties_display_section2') == 1): ?>
    <div class="uk-block uk-block-gradient uk-contrast">
	    <div class="uk-container uk-container-center uk-text-center">
	      <h4><?php echo get_field('parties_header'); ?></h4>
	      <div class="costs uk-grid uk-grid-width-1-2">
	        <div class="option-1">
	          <h5><?php echo get_field('parties_sub1_header'); ?></h5>
	          <h6><?php echo get_field('parties_sub1_sub _header'); ?></h6>
	          <?php echo get_field('parties_sub1_content'); ?>
	        </div>
	        <div class="option-2 uk-border-left">
	          <h5><?php echo get_field('parties_sub2_header'); ?></h5>
	          <h6><?php echo get_field('parties_sub2_sub _header'); ?></h6>
	          <?php echo get_field('parties_sub1_content'); ?>
	        </div>
	      </div>
	      <p class="more-info"><?php echo get_field('parties_footer'); ?></p>
	    </div>
  	</div>
<?php endif; ?>
</section>