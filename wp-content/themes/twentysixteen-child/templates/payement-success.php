<?php
/**
 * Template Name: Payment Success Page
 */
?>

<?php 
$_SESSION['payment_id'] = $_REQUEST['charge'];


$client_message = '<htm><div dir=ltr><div></div><div>Thanks<br></div><div><br></div><div><br></div><div><div><div><div><br></div><div><div><div>';
$client_message .= $_SESSION['email_content']['email_body'];
$client_message .= '<div><br></div><div><br></div><div><br></div><div>Thank you for booking your child’s place onto our Summer Workshops. <br></div><div>Please remember to send them with a packed lunch and plenty of drinks and snacks for the the day. <br></div><div><br></div><div><br></div><div><br></div><div><br></div><div><div><br></div><div><div style=color:#000;font-family:Helvetica;font-size:14px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:normal;text-indent:0;text-transform:none;white-space:normal;word-spacing:0><div style=color:#000;font-family:Helvetica;font-size:14px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:normal;text-indent:0;text-transform:none;white-space:normal;word-spacing:0><span style=float:none;display:inline>Many Thanks and with Best Wishes,</span><br></div><div style=color:#000;font-family:Helvetica;font-size:14px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:normal;text-indent:0;text-transform:none;white-space:normal;word-spacing:0><div><span style=float:none;display:inline>Laura Keeling</span><br></div><div><span style=float:none;display:inline>Studio Director</span><br></div><div><span style=float:none;display:inline>Rooftop Studios Performing Arts</span><span style=float:none;display:inline> </span><span style=float:none;display:inline>School,</span><span style=float:none;display:inline> </span><br></div><div><span style=float:none;display:inline>High Street Arcade, Stone, Staffs,</span><span style=float:none;display:inline> </span><span style=float:none;display:inline>ST15 8AU</span><br></div><div><span style=float:none;display:inline><a href=http://www.rooftopstudios.co.uk target=_blank>www.rooftopstudios.co.uk</a></span><br></div><div><span style=float:none;display:inline>01785 761233 / 07966 539 555</span><br></div></div><div style=color:#000;font-family:Helvetica;font-size:14px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:normal;text-indent:0;text-transform:none;white-space:normal;word-spacing:0><span style=float:none;display:inline></span><br></div></div><div><br></div><div><span style=color:#000><span style=font-family:Helvetica><span style=font-size:14px><span><img height=200 src=https://www.rooftopstudios.co.uk/wp-content/uploads/2017/07/rt_email_template.jpg width=200></span></span></span></span><br></div></div><div><br></div></div>';

$headers = array('Content-Type: text/html; charset=UTF-8'); //'From: webmaster@rooftopstudios.co.uk'
$to = $_SESSION['email_content']['email_id'];
$subject = 'Workshop confirmation';
$message = $client_message; 
wp_mail($to,$subject,$message,$headers);
  
add_post_meta($_SESSION["post_id"],'workshop_submission_posted-payment_id', $_REQUEST['charge']);

$url = $_SESSION['workshopurl'];
wp_redirect($url);
exit;
?>

