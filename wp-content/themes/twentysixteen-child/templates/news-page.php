<?php
/**
 * Template Name: News Page
 */
?>
<?php get_header(); ?>

<section id="content" <?php post_class();?>>
	<div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center">
      <div class="believe uk-text-center">
        <h3><?php the_title();?></h3>
        <?php the_excerpt();?>
      </div>
    </div>
  </div>
  <div class="uk-block">
    <div class="uk-container uk-container-center">
      <div class="uk-block-birds"></div>
      <div class="main-content">
      <div class="uk-grid">
            <div class="">
              <!-- repeatable-->
                <?php 

             $args = array('post_type' => 'news');
      		   $query = new WP_Query($args);
      	    while ($query->have_posts()) : $query->the_post();
                 ?>
                <div class="class" data-grid-prepared="true" aria-hidden="false" >
                  <div class="uk-panel uk-panel-box" style="padding: 0px 25px;">
                    <a href="<?php the_permalink();?>" data-post-id="<?php the_ID(); ?>" class="uk-position-cover"></a>
                    <div class="uk-panel-teaser"><?php the_post_thumbnail(); ?></div>
                    <h6 class="uk-text-truncate"><?php the_title(); ?></h6>
                    <p class="uk-text-clamp"><?php echo get_the_excerpt(); ?></p>
                  </div>
                </div> 
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
              <!-- repeatable-->

            </div>
        <div class="uk-width-medium-1-4">
          <div class="twiiter-sidebar">
                <a class="twitter-timeline" data-height="500" href="<?php echo get_field('twitter_embeded_url');?>" style="position: static; visibility: visible; display: inline-block; width: 900px; height: 500px; padding: 0px; border: none; max-width: 100%; min-width: 180px; margin-top: 0px; margin-bottom: 0px; min-height: 200px;" >Tweets by RooftopStudios1</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if(get_field('parties_display_section2') == 1): ?>
    <div class="uk-block uk-block-gradient uk-contrast">
	    <div class="uk-container uk-container-center uk-text-center">
	      <h4><?php echo get_field('parties_header'); ?></h4>
	      <div class="costs uk-grid uk-grid-width-1-2">
	        <div class="option-1">
	          <h5><?php echo get_field('parties_sub1_header'); ?></h5>
	          <h6><?php echo get_field('parties_sub1_sub _header'); ?></h6>
	          <?php echo get_field('parties_sub1_content'); ?>
	        </div>
	        <div class="option-2 uk-border-left">
	          <h5><?php echo get_field('parties_sub2_header'); ?></h5>
	          <h6><?php echo get_field('parties_sub2_sub _header'); ?></h6>
	          <?php echo get_field('parties_sub1_content'); ?>
	        </div>
	      </div>
	      <p class="more-info"><?php echo get_field('parties_footer'); ?></p>
	    </div>
  	</div>
<?php endif; ?>
</section>
<?php get_footer(); ?>