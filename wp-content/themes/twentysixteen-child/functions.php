<?php
session_start();

function rts_setup() {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(1200, 9999);

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus(array(
        'primary' => __('Primary Menu', 'twentysixteen'),
        'social' => __('Social Links Menu', 'twentysixteen'),
        'footer' => __('Footer Menu', 'twentysixteen'),
        'responsive-nav' => __('Responsive Menu', 'twentysixteen'),
    ));
}

add_action('after_setup_theme', 'rts_setup');

function rts_theme_enqueue_styles() {
    wp_enqueue_style('reset', get_stylesheet_directory_uri() . '/css/reset.min.css');
    wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('custom-css', get_stylesheet_directory_uri() . '/css/styles.min.css');
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style('bootstrap-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');

    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/jquery.js', array('jquery'), '2.1.4', true);
    wp_enqueue_script('uikit.min', get_stylesheet_directory_uri() . '/js/uikit.min.js', array('jquery'), '2.24.3', true);
    wp_enqueue_script('custom', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '2.24.3', true);
    wp_enqueue_script('slider.min', get_stylesheet_directory_uri() . '/js/components/slider.min.js', array('uikit.min'), '2.24.3', true);
    wp_enqueue_script('slideshow.min', get_stylesheet_directory_uri() . '/js/components/slideshow.min.js', array('uikit.min'), '2.24.3', true);
    wp_enqueue_script('slideshow-fx.min', get_stylesheet_directory_uri() . '/js/components/slideshow-fx.min.js', array('uikit.min'), '2.24.3', true);
    wp_enqueue_script('grid.min', get_stylesheet_directory_uri() . '/js/components/grid.min.js', array('uikit.min'), '2.24.3', true);
    wp_enqueue_script('sticky.min', get_stylesheet_directory_uri() . '/js/components/sticky.min.js', array('uikit.min'), '2.24.3', true);
    
    wp_enqueue_script('bootstrap-min-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('uikit.min'), '3.3.7', true);
}
    
add_action('wp_enqueue_scripts', 'rts_theme_enqueue_styles');

/**
 * Custom admin login header logo
 */
function rts_custom_login_logo() {
    echo '<style type="text/css">' . 'h1 a { background-image:url(' . get_bloginfo('template_directory') . '/images/logo.jpg) !important;
            background-size: 100% !important;
            height: 120px !important;
            width: 100% !important;
     }
     body{
        background: hsla(199, 81%, 54%, 0.38) !important;
     }
     .login form{
        box-shadow: 0 0 20px 10px rgba(0,0,0,.13)!important;
     }
     ' . '</style>';
}

add_action('login_head', 'rts_custom_login_logo');

function rts_add_excerpts_to_pages() {
    add_post_type_support('page', 'excerpt');
}

add_action('init', 'rts_add_excerpts_to_pages');

/* -------------------Create Custom Post ------start------------------ */

function rts_custom_init() {

    $labels = array(
        'name' => _x('Workshops', 'post type general name', 'Workshop-textdomain'),
        'singular_name' => _x('Workshop', 'post type singular name', 'Workshop-textdomain'),
        'menu_name' => _x('Workshops', 'admin menu', 'Workshop-textdomain'),
        'name_admin_bar' => _x('Workshop', 'add new on admin bar', 'Workshop-textdomain'),
        'add_new' => _x('Add New', 'Workshop', 'Workshop-textdomain'),
        'add_new_item' => __('Add New Workshop', 'Workshop-textdomain'),
        'new_item' => __('New Workshop', 'Workshop-textdomain'),
        'edit_item' => __('Edit Workshop', 'Workshop-textdomain'),
        'view_item' => __('View Workshop', 'Workshop-textdomain'),
        'all_items' => __('All Workshops', 'Workshop-textdomain'),
        'search_items' => __('Search Workshops', 'Workshop-textdomain'),
        'parent_item_colon' => __('Parent Workshops:', 'Workshop-textdomain'),
        'not_found' => __('No Workshops found.', 'Workshop-textdomain'),
        'not_found_in_trash' => __('No Workshops found in Trash.', 'Workshop-textdomain'),
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'Workshop-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'Workshop'),
        'capability_type' => 'post',
        'has_archive' => true,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions'),
        'taxonomies' => array('category'),
    );

    register_post_type('Workshop', $args);
    add_action('init', 'rts_add_category_taxonomy_to_Workshop');
}

function rts_add_category_taxonomy_to_Workshop() {
    register_taxonomy_for_object_type('category', 'Workshop');
}

add_action('init', 'rts_custom_init');

function rts_custom2_init() {

    $labels = array(
        'name' => _x('Courses', 'post type general name', 'Course-textdomain'),
        'singular_name' => _x('Course', 'post type singular name', 'Course-textdomain'),
        'menu_name' => _x('Courses', 'admin menu', 'Course-textdomain'),
        'name_admin_bar' => _x('Course', 'add new on admin bar', 'Course-textdomain'),
        'add_new' => _x('Add New', 'Course', 'Course-textdomain'),
        'add_new_item' => __('Add New Course', 'Course-textdomain'),
        'new_item' => __('New Course', 'Course-textdomain'),
        'edit_item' => __('Edit Course', 'Course-textdomain'),
        'view_item' => __('View Course', 'Course-textdomain'),
        'all_items' => __('All Courses', 'Course-textdomain'),
        'search_items' => __('Search Courses', 'Course-textdomain'),
        'parent_item_colon' => __('Parent Courses:', 'Course-textdomain'),
        'not_found' => __('No Courses found.', 'Course-textdomain'),
        'not_found_in_trash' => __('No Courses found in Trash.', 'Course-textdomain'),
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'Course-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'Course'),
        'capability_type' => 'post',
        'has_archive' => true,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions'),
        'taxonomies' => array('category'),
    );

    register_post_type('Course', $args);
    add_action('init', 'rts_add_category_taxonomy_to_Course');
}

function rts_add_category_taxonomy_to_Course() {
    register_taxonomy_for_object_type('category', 'Course');
}

add_action('init', 'rts_custom2_init');

function rts_custom3_init() {

    $labels = array(
        'name' => _x('News', 'post type general name', 'News-textdomain'),
        'singular_name' => _x('News', 'post type singular name', 'News-textdomain'),
        'menu_name' => _x('News', 'admin menu', 'News-textdomain'),
        'name_admin_bar' => _x('News', 'add new on admin bar', 'News-textdomain'),
        'add_new' => _x('Add News', 'News', 'News-textdomain'),
        'add_new_item' => __('Add New News', 'News-textdomain'),
        'new_item' => __('New News', 'News-textdomain'),
        'edit_item' => __('Edit News', 'News-textdomain'),
        'view_item' => __('View News', 'News-textdomain'),
        'all_items' => __('All News', 'News-textdomain'),
        'search_items' => __('Search News', 'News-textdomain'),
        'parent_item_colon' => __('Parent News:', 'News-textdomain'),
        'not_found' => __('No News found.', 'News-textdomain'),
        'not_found_in_trash' => __('No News found in Trash.', 'News-textdomain'),
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'News-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'News'),
        'capability_type' => 'post',
        'has_archive' => true,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions'),
        'taxonomies' => array('category'),
    );

    register_post_type('News', $args);
    add_action('init', 'rts_add_category_taxonomy_to_New');
}

function rts_add_category_taxonomy_to_New() {
    register_taxonomy_for_object_type('category', 'News');
}

add_action('init', 'rts_custom3_init');

/* -------------------Create Custom Post ------End------------------ */

/* -------------------Customize Woo For This Theme------start------------------ */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10);
remove_action('woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10);

function sil_woocommerce_template_loop_category_title($category) {
    $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
    $image = wp_get_attachment_url($thumbnail_id);
    if ($image) {
        echo '<img src="' . $image . '" alt="' . $category->name . '" />';
    } else {
        echo '<img src="' . wc_placeholder_img_src() . '" alt="' . $category->name . '" />';
    }
    echo $category->name;
}

add_action('woocommerce_shop_loop_subcategory_title', 'sil_woocommerce_template_loop_category_title', 9);

/* -------------------Customize Woo For This Theme------end-------------------- */

/* Remoce add to cart button */

function remove_loop_button() {
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
}

add_action('init', 'remove_loop_button');

/* Modify and add view button */

add_action('woocommerce_after_shop_loop_item', 'replace_add_to_cart');

function replace_add_to_cart() {
    global $product;
    $link = $product->get_permalink();
    echo '<a href="' . esc_attr($link) . '" class="view-button button">View</a>';
}

add_action('woocommerce_before_main_content', 'remove_sidebar');

function remove_sidebar() {
    // if (is_checkout() || is_cart() || is_product()) {
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
    // }
}

/* get workshop details */

function get_workshop_data() {
    $pid = intval($_POST['postid']);
    $post = get_post($pid);
    setup_postdata($post);


    if ($post) {
        $hour_price = "";
        
        while (has_sub_field("workshop_property", $post->ID)) {
        $price = get_sub_field('workshop_price');
        $time = get_sub_field('workshop_time');
        $color = get_sub_field('workshop_button_color');
         if ($price || $time || $color) {
            $hour_price .= '
	    <div class="hour">
		<p class="price">£' . get_sub_field('workshop_price') . '</p>
		<!--<a href="' . get_page_link(422) . '?workshop-id=' . $post->ID . '" class="uk-button blue uk-width-1-1" style="background-color:' . get_sub_field('workshop_button_color') . ' !important">' . get_sub_field('workshop_time') . '</a>-->
		<a href="' . get_permalink($post->ID) . '" class="uk-button blue uk-width-1-1" style="background-color:' . get_sub_field('workshop_button_color') . ' !important">' . get_sub_field('workshop_time') . '</a>
	    </div>
		';
        } }

        $data = '
            <div class="workshop-modals-content uk-text-center">
		<div class="uk-panel uk-panel-body">
		    <h3>' . $post->post_title . '</h3>
		    <p>' . $post->post_content . '</p>
		    <p>' . $post->post_excerpt . '</p>
		    <div class="uk-grid uk-grid-width-1-3 price-block">
		    ' . $hour_price . '
		    </div>
		</div>
	    </div>';
    } else {
        echo '<div id="postdata">' . __('Didn`t find anything') . '</div>';
    }
    wp_reset_postdata();


    echo $data;
    exit;
}

add_action('wp_ajax_get_workshop_data', 'get_workshop_data');
add_action('wp_ajax_nopriv_get_workshop_data', 'get_workshop_data'); /* get workshop details */

/* get course details */

function get_course_data() {
    $pid = intval($_POST['postid']);
    $post = get_post($pid);
    setup_postdata($post);


    if ($post) {
        $hour_price = "";
        $i_course_fee_structure = 0;
        while (has_sub_field("course_fee_structure", $post->ID)) {
        $courseprice = get_sub_field('course_price');
        $coursetime = get_sub_field('course_time');
        $coursecolor = get_sub_field('course_button_color');
        if ($courseprice || $coursetime || $coursecolor) {
                switch ($i_course_fee_structure) {
                    case 0:
                        $hour_price .= '<div class="hour">
    						<p class="price">£' . get_sub_field('course_price') . '</p>
    						<a href="' . get_page_link(22) . '" class="uk-button lilac uk-width-1-1" style="background-color:' . get_sub_field('course_button_color') . ' !important">' . get_sub_field('course_time') . '</a>
    					    </div>';
                        break;
                    case 1:
                        $hour_price .= '<div class="half-day">
    						<p class="price">£' . get_sub_field('course_price') . '</p>
    						<a href="' . get_page_link(330) . '?courseId=' . $post->ID . '" class="uk-button turquoise uk-width-1-1" style="background-color:' . get_sub_field('course_button_color') . ' !important">' . get_sub_field('course_time') . '</a>
    					    </div>';
                        break;
                    case 2:
                        $hour_price .= '<div class="full-day">
    						<p class="price">£' . get_sub_field('course_price') . '</p>
    						<a class="uk-button fuchsia uk-width-1-1 pay-term-now" style="background-color:' . get_sub_field('course_button_color') . ' !important">' . get_sub_field('course_time') . '</a>
    					    </div>';
                        $price = get_sub_field('course_price');
                        $price *= 100;
                        echo '<style>
    								.stripe-button-el{
    									display: none !important;
    								}
    						</style>';
                        echo do_shortcode('[stripe name="Roof Top Studio" description="Term Fees" amount=' . $price . ' currency="GBP"]');
                        break;
                    default:
                        $hour_price .= '<div class="hour">
    						<p class="price">£' . get_sub_field('course_price') . '</p>
    						<a class="uk-button fuchsia uk-width-1-1" style="background-color:' . get_sub_field('course_button_color') . ' !important">' . get_sub_field('course_time') . '</a>
    					    </div>';
                        break;
                }
                $i_course_fee_structure++;
            }
        }

        $category_detail = get_the_category($post->ID); //$post->ID

        foreach ($category_detail as $cd) {
            $cat_name = $cd->name;
        }

        echo get_the_post_thumbnail($post, 'full');
        $data = '
        <div class="workshop-modals-content uk-text-center">
		<div class="uk-panel uk-panel-body">
		    <h3>' . $post->post_title . '</h3>
		    <p class="uk-text-bold">Age Group: <span class="uk-text-orange">' . $cat_name . '</span></p>
		    <p>' . $post->post_content . '</p>
		    <div class="uk-grid uk-grid-width-1-3 price-block">
		    ' . $hour_price . '
		    </div>
		</div>
	    </div>';
    } else {
        echo '<div id="postdata">' . __('Didnt find anything') . '</div>';
    }
    wp_reset_postdata();


    echo $data;
    exit;
}

add_action('wp_ajax_get_course_data', 'get_course_data');
add_action('wp_ajax_nopriv_get_course_data', 'get_course_data');

add_action('admin_bar_menu', 'remove_wp_logo', 999);

function remove_wp_logo($wp_admin_bar) {
    $wp_admin_bar->remove_node('wp-logo');
}

function send_enrolment_mail() {
    $pid = $_POST['courseId'];
    // var_dump($pid,$_REQUEST);
    // $to = 'testing.blinkdigital@gmail.com';
    $to = 'info@rooftopstudios.co.uk';
    $subject = 'Request For New Enrolment';
    $message = 'Please follow details';
    $message .= '<br/>';
    $message .= 'Child Name:' . $_POST['child_name'] . '<br/>';
    $message .= 'Child Age:' . $_POST['child_age'] . '<br/>';
    $message .= 'Child DOB:' . $_POST['child_dob'] . '<br/>';
    $message .= 'Parent/Guardian name:' . $_POST['parent_name'] . '<br/>';
    $message .= 'Parent / Guardian Mob number:' . $_POST['parent_mobile'] . '<br/>';
    $message .= 'Parent email address:' . $_POST['parent_email'] . '<br/>';
    $message .= 'Class you would like to try:' . $_POST['like_to_try'] . '<br/>';
    $message .= 'Date of trial:' . $_POST['date_of_trial'] . '<br/>';
    $message .= 'Time of class:' . $_POST['time_of_class'] . '<br/>';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    echo wp_mail($to, $subject, $message, $headers);
    exit;
}

add_action('wp_ajax_send_enrolment_mail', 'send_enrolment_mail');
add_action('wp_ajax_nopriv_send_enrolment_mail', 'send_enrolment_mail');

function send_workshop_register_mail() {

    $data = array();
    parse_str($_POST['data'], $data);
    // $to = 'testing.blinkdigital@gmail.com';
    $to = 'info@rooftopstudios.co.uk';
    $subject = 'Request For Workshop Booking';
    $message = 'Please follow details';
    $message .= '<br/>';
    $message .= 'Workshop : ' . $data['workshop_name'] . '<br/>';
    $message .= 'Date : ' . $data['workshop_date'] . '<br/>';
    $message .= 'Child Name : ' . $data['child_name'] . '<br/>';
    $message .= 'Parent/Guardian Name : ' . $data['parent_name'] . '<br/>';
    $message .= 'Child Age : ' . $data['child_age'] . '<br/>';
    $message .= 'Child DOB : ' . $data['child_dob'] . '<br/>';
    $message .= 'Address : ' . $data['child_address'] . '<br/>';
    $message .= 'Email : ' . $data['child_email'] . '<br/>';
    $message .= 'Emergency Contact Name 1 : ' . $data['emergncy_name1'] . '<br/>';
    $message .= 'Emergency Contact Name 2 : ' . $data['emergncy_name2'] . '<br/>';
    $message .= 'Emergency Tel Number 1 : ' . $data['emergncy_contact1'] . '<br/>';
    $message .= 'Emergency Tel Number 2 : ' . $data['emergncy_contact2'] . '<br/>';
    $message .= 'Medical Condition : ' . $data['medical_condtn'] . '<br/>';
    $message .= 'I Consent To The Use Of Face Paints : ' . $data['face_paints'] . '<br/>';
    $message .= 'I Consent To RT Studios Taking Photos/Videos During The Workshop For Promo Use. : ' . $data['photos'] . '<br/>';
    $message .= 'Booking Type : ' . $data['day_booking'] . '<br/>';
    $message .= 'Total Price : ' . $data['price'] . '<br/>';
    $message .= 'Early Drop : ' . $data['early_drop'] . '<br/>';
    $message .= 'Late Pick Up : ' . $data['late_pickup'] . '<br/>';
    $params = array('name' => $data['workshop_name'], 'date' => $data['workshop_date']);
    $s_message = workshop_email_template($params);
    $client_mail = array('email_id' => $data['child_email'], 'email_body' => $s_message);
    $_SESSION['email_content'] = $client_mail;
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $data['price'] *= 100;

    do_action('before_workshop_wp_mail', compact('to', 'subject', 'message', 'headers'), $data);
    if (wp_mail($to, $subject, $message, $headers)) {

        echo do_shortcode('[stripe name="Roof Top Studio" description="Workshop Fees" amount=' . $data['price'] . ' currency="GBP"]');
        exit;
    }
}

add_action('wp_ajax_send_workshop_register_mail', 'send_workshop_register_mail');
add_action('wp_ajax_nopriv_send_workshop_register_mail', 'send_workshop_register_mail');

function rts_workshop_submission() {

    $labels = array(
        'name' => __('Workshop Form Submissions', 'Workshop-textdomain'),
        'singular_name' => __('Submission', 'Workshop-textdomain'),
        'menu_name' => __('Submission', 'Workshop-textdomain'),
        'all_items' => __('Submissions', 'Workshop-textdomain'),
        'view_item' => __('Submission', 'Workshop-textdomain'),
        'edit_item' => __('Submission', 'Workshop-textdomain'),
        'search_items' => __('Search', 'Workshop-textdomain'),
        'not_found' => __('Not found', 'Workshop-textdomain'),
        'not_found_in_trash' => __('Not found in Trash', 'Workshop-textdomain'),
    );
    $args = array(
        'label' => __('Submission', 'Workshop-textdomain'),
        'description' => __('Post Type Description', 'Workshop-textdomain'),
        'labels' => $labels,
        'supports' => false,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => 'edit.php?post_type=workshop',
        'show_in_admin_bar' => false,
        'show_in_nav_menus' => false,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'rewrite' => false,
        'capability_type' => 'page',
        'query_var' => false,
        'capabilities' => array(
            'create_posts' => false
        ),
        'map_meta_cap' => true
    );
    register_post_type('workshop_submission', $args);
}

add_action('init', 'rts_workshop_submission');

function rts_workshop_submission_save($mailData, $postData) {

    $post = array(
        'post_title' => ' ',
        'post_content' => $mailData['message'],
        'post_status' => 'publish',
        'post_type' => 'workshop_submission',
    );

    if (isset($mailData['parent'])) {
        $post['post_parent'] = $mailData['parent'];
    }

    $post_id = wp_insert_post($post);

    // check the post was created
    if (!empty($post_id) && !is_wp_error($post_id)) {

        add_post_meta($post_id, 'to', $mailData['to']);
        add_post_meta($post_id, 'subject', $mailData['subject']);
        add_post_meta($post_id, 'message', $mailData['message']);
        add_post_meta($post_id, 'headers', $mailData['headers']);

        if (!empty($postData)) {
            foreach ($postData as $name => $value) {
                if (!empty($value)) {
                    add_post_meta($post_id, 'workshop_submission_posted-' . $name, $value);
                }
            }
        }
    }
    $_SESSION["post_id"] = $post_id;

    return $post_id;
}

add_action('before_workshop_wp_mail', 'rts_workshop_submission_save', 10, 2);

function workshop_submission_column($column, $post_id) {
    $form_id = get_post_meta($post_id);
    $post_parent = wp_get_post_parent_id($post_id);
    $nested = ($post_parent > 0) ? '&mdas h; ' : '';

    switch ($column) {

        case 'form':
            ?><a
                href="<?php echo add_query_arg(array('page' => 'workshop_submission', 'post' => $form_id, 'action' => 'edit'), admin_url('admin.php')); ?>"><?php echo get_the_title($form_id); ?></a><?php
                break;
            case 'sent':
                ?><a
                href="<?php echo add_query_arg(array('page' => 'workshop_submission', 'post' => $form_id, 'action' => 'edit'), admin_url('admin.php')); ?>"><?php echo get_the_title($form_id); ?></a><?php
                break;
            case 'submission':
                ?>
            <strong>
                <a class="row-title" href="<?php echo get_edit_post_link($post_id); ?>">
                    <?php echo $nested . htmlspecialchars(get_post_meta($post_id, 'sender', true)); ?>
                </a>
            </strong>
            <?php
            break;
        case 'workshop_submission_posted-price':
            echo '£' . get_post_meta($post_id, $column, true) / 100;
            break;
        default:
            echo get_post_meta($post_id, $column, true);
            break;
    }
}

add_action('manage_workshop_submission_posts_custom_column', 'workshop_submission_column', 10, 2);

/**
 * Change the default table columns
 */
function workshop_submission_set_columns($columns) {
    $columns = array(
        'cb' => '<input type="checkbox">',
        'workshop_submission_posted-workshop_name' => __('Workshop', 'Workshop-textdomain'),
        'workshop_submission_posted-child_name' => __('Child Name', 'Workshop-textdomain'),
        'workshop_submission_posted-price' => __('Fees', 'Workshop-textdomain'),
        'workshop_submission_posted-payment_id' => __('PaymentID', 'Workshop-textdomain'),
    );

    // dynamically add cols if the user selects a form
    if (isset($_GET['workshop_submission']) && !empty($_GET['workshop_submission'])) {
        $form_id = $_GET['workshop_submission'];

        $workshop_submission_columns = workshop_submission_available_columns($form_id);

        foreach ($workshop_submission_columns as $meta_key) {
            $columns[$meta_key] = str_replace('workshop_submission_posted-', '', $meta_key);
        }
    }

    $columns['date'] = __('Date', 'Workshop-textdomain');
    return $columns;
}

add_filter('manage_workshop_submission_posts_columns', 'workshop_submission_set_columns', 999);

/**
 * Get the fields from a form
 *
 * @param  integer $form_id the form post ID
 *
 * @return array|boolean    the form keys
 */
function _available_columns($form_id = 0) {
    global $wpdb;

    $post_id = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'form_id' AND meta_value = $form_id LIMIT 1;");

    $columns = $wpdb->get_col("SELECT meta_key FROM wp_postmeta WHERE post_id = $post_id AND meta_key LIKE '%workshop_submission_%' GROUP BY meta_key");
    return $columns;
}

function workshop_submission_meta_boxes() {
    global $post_id;
    add_meta_box('workshop_submission_mail', __('Full Details of Workshop', 'Workshop-textdomain'), 'workshop_submission_mail_meta_box', 'workshop_submission', 'normal');

    add_meta_box('workshop_submission_actions', __('Overview', 'Workshop-textdomain'), 'workshop_submission_actions_meta_box', 'workshop_submission', 'side');

    remove_meta_box('submitdiv', 'workshop_submission', 'side');
}

add_action('add_meta_boxes', 'workshop_submission_meta_boxes', 25);

function workshop_submission_mail_meta_box($post) {
    ?>

    <table class="form-table contact-form-submission">
        <tbody>
            <tr>
                <th scope="row"><?php _e('Subject', 'contact-form-submissions'); ?></th>
                <td><?php echo get_post_meta($post->ID, 'subject', true); ?></td>
            </tr>
            <tr>
                <th scope="row"><?php _e('Body', 'contact-form-submissions'); ?></th>
                <td><?php echo apply_filters('the_content', $post->post_content); ?></td>
            </tr>
        </tbody>
    </table>

    <?php
}

/**
 * Output for the actions metabox
 */
function workshop_submission_actions_meta_box($post) {
    $datef = __('M j, Y @ H:i');
    $date = date_i18n($datef, strtotime($post->post_date));
    ?>
    <div id="minor-publishing">

        <div id="misc-publishing-actions">
            <div class="misc-pub-section curtime misc-pub-curtime">
                <span id="timestamp"><?php _e('Submitted', 'contact-form-submissions'); ?> : <b><?php echo $date; ?></b></span>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <?php
}

/**
 * Display category image on category archive
 */
add_action('woocommerce_archive_description', 'woocommerce_category_image', 2);

function woocommerce_category_image() {
    if (is_product_category()) {
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
        $image = wp_get_attachment_url($thumbnail_id);
        if ($image) {
            echo '<img src="' . $image . '" alt="' . $cat->name . '" />';
        }
    }
}

function workshop_email_template($params = array()) {
    $chlidplace = "You have successfully booked your child's place on our";

    $html = '<!DOCTYPE html>';
    $html .= '<html lang="en-US">';
    $html .= '<head>';
    $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
    $html .= '<title>rooftopstudios</title>';
    $html .= '</head>';
    $html .= '<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">';
    $html .= '<div id="wrapper" dir="ltr"
     style="background-color: #f7f7f7; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">';
    $html .= '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">';
    $html .= '<tr>';
    $html .= '<td align="center" valign="top">';
    $html .= '<div id="template_header_image"></div>';
    $html .= '<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container"
                       style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #ffffff; border: 1px solid #dedede; border-radius: 3px !important;">';
    $html .= '<tr><td align="center" valign="top">';
    $html .= '<table  bgcolor="#5F80F3" border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="background: #5F80F3; border-radius: 3px 3px 0 0 !important; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; , Helvetica, Roboto, Arial, sans-serif;">';
    $html .= '<tr><td id="header_wrapper" style="padding: 10px; display: block;">';
    $html .= '<h1 style="color: #ffffff;text-align: center; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-shadow: 0 1px 0 #5F80F3; -webkit-font-smoothing: antialiased;">';
    $html .= '<img class="img-responsive" style="width: 350px;height: 120px;" src="https://www.rooftopstudios.co.uk/wp-content/uploads/2017/04/logo.jpg">';
    $html .= '</h1></td></tr></table></td></tr>';
    $html .= '<tr><td align="center" valign="top">';
    $html .= '<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body"><tr>';
    $html .= '<td valign="top" id="body_content" style="background-color: #ffffff;">';
    $html .= '<table border="0" cellpadding="20" cellspacing="0" width="100%">';
    $html .= '<tr><td valign="top" style="padding: 48px;">';
    $html .= '<div id="body_content_inner" style="color: #636363; font-size: 14px; line-height: 150%; text-align: left;">';
    $html .= '<p style="margin: 0 0 16px;">' . $chlidplace . '</p>';
    $html .= '<p style="margin: 0 0 16px;">' . $params['name'] . ' for ' . $params['date'] . '</p>';
    $html .= '<p style="margin: 0 0 16px;">Please may we remind you that your child will need a packed lunch and ample snacks and drinks for the day. </p>';
    $html .= '<p style="margin: 0 0 16px;">Please feel free to wear costumes/dress up according to the theme of the workshop, optional not compulsory. </p>';
    $html .= '<p style="margin: 0 0 16px;">We also have before and after care available from 8.30am- 16.30pm at £3.50 an hour. Please note this service is only available provided we have a minimum of 4 children. Confirmation of before and aftercare will be sent out 5 days prior to Workshop. </p>';
    $html .= '<p style="margin: 0 0 16px;">Any children with asthma please bring inhaler with you, clearly named. </p>';
    $html .= '<p style="margin: 0 0 16px;">We look forward to seeing you and meeting your child at the Workshop! </p>';
    $html .= '<p style="margin: 0 0 16px;">Many Thanks, </p>';
    $html .= '<p style="margin: 0 0 16px;">Rooftop Studios </p>';
    $html .= '</div></td></tr></table>';
    $html .= '</td></tr></table></td></tr><tr>';
    $html .= '<td align="center" valign="top">';
    $html .= '<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">';
    $html .= '<tr><td valign="top" style="padding: 0; -webkit-border-radius: 6px;">';
    $html .= '<table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
    $html .= '<td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #c09bb9; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">';
    /*    $html .= '<a href="https://www.rooftopstudios.co.uk" style="text-decoration: none">rooftopstudios</a>'; */
    $html .= '</td></tr></table></td></tr></table>';
    $html .= '</td></tr></table></td></tr></table></div></body></html>';

    return $html;
}

add_action('woocommerce_thankyou', 'so_payment_complete');

function so_payment_complete($order_id) {

    $order = wc_get_order($order_id);
    $user = $order->get_user();
    ?>
<style>
    #myModal.modal{
        top:30%;
    }
    </style>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Order Placed</h4>
                </div>
                <div class="modal-body">
                    <p>Thank you for your order, it will be ready to collect from Rooftop Studios within 7 days.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    
    
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#myModal').modal('show');

        });
    </script>
    <?php
}

function your_function() {
?>
 <script type="text/javascript">
 document.addEventListener( 'wpcf7submit', function( event ) {
 ga( 'send', 'event', 'contact', 'submit' );
 }, false );
 </script>
<?php 
}
add_action( 'wp_footer', 'your_function' );

 
