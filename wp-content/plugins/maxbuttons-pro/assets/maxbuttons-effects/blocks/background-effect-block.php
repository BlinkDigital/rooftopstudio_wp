<?php namespace MaxButtons;
defined('ABSPATH') or die('No direct access permitted');

$blockClass["gradient"] = "backgroundBlock";
//$blockOrder[0][] = "basic";

// Hover EFFECTS when switching from normal to hover.
// https://fontawesome.com/how-to-use/on-the-web/styling/power-transforms
class backgroundBlock extends gradientBlock
{

  //  protected $fields = array();

    public function __construct()
    {
        $this->fields = array_merge($this->fields, array(
                  'background_transition' => array('default' => '0'),
        ));
    }

    public function parse_css($css,  $mode = 'normal')
    {
      $css = parent::parse_css($css);
      $data = $this->data[$this->blockname];

      $transition = maxBlocks::getValue('background_transition');

      // include a check here to see if gradient is activated.
      if ($transition > 0)
      {
         $css["maxbutton"]["normal"]['transition'] = 'background-color ' . $transition . 'ms linear';
         $css["maxbutton"]["hover"]['transition'] = 'background-color ' . $transition . 'ms linear';
      }

      return $css;
    }

    public function admin_fields()
    {
      parent::admin_fields();

      $conditional = htmlentities(json_encode(array('target' => 'use_gradient', 'values' => 'unchecked')));

      $data = $this->data[$this->blockname];
      $admin = MB()->getClass('admin');

      $transition = new maxField('number');
      $transition->id = 'background_transition';
      $transition->name = $transition->id;
      $transition->min = 0;
      $transition->value = maxBlocks::getValue($transition->id);
      $transition->label = __('Background Transition (in MS)');
      $transition->help = __('Time it takes to fade to hover status. 1 Second is 1000 ms', 'maxbuttons-pro');
      $transition->start_conditional = $conditional;
      $admin->insertField('gradient_stop', $transition, 'start', 'end', 'after');

    }

  /*  public function sidebar()
    {
        $data = $this->data[$this->blockname];
        $admin = MB()->getClass('admin');

        $startbar = new maxField('sidebar_start');
        $startbar->title = __('Hover Effects', 'maxbuttons-pro');
        $startbar->label = __('Effects', 'maxbuttons-pro');
        $admin->addField($startbar);

        $sidebarend = new maxField('sidebar_end');

        $admin->addField($sidebarend);

    }
*/

} // class
