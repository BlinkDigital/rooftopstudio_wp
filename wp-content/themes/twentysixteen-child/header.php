<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="tkNzbXIB7jz3RcGiXdEHJqQscS7ZsI0XI9Hk8fgTMX4" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if (is_singular() && pings_open(get_queried_object())) : ?>
    	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
    </head>
    <?php $frontpage_id = get_option('page_on_front'); ?>
    <body <?php body_class(); ?> onload="paint()" onresize="paint()">

	<script type="text/javascript">
	    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	</script>

	<section id="header">
	    <div id="header-first" class="uk-block uk-block-gradient uk-block-header uk-block-collapsed uk-contrast">
		<div class="uk-container uk-container-center">
		    <div class="uk-grid">
			<div class="uk-width-medium-1-2 logo"><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_field('logo', $frontpage_id); ?>" alt="rooftopstudio" /></a></div>
			<div class="uk-width-medium-1-2 uk-float-right uk-hidden-small info">
			    <div class="uk-width-1-1 uk-hidden-small uk-panel-body uk-flex uk-flex-middle uk-flex-right">
				<span class="uk-margin-right">T: <?php echo get_field('contact_no', $frontpage_id); ?></span>
				<a href="<?php echo get_field('facebook_link', $frontpage_id); ?>" target="_blank" class="uk-icon-button uk-icon-small uk-icon-facebook"></a>
				<a href="<?php echo get_field('twitter_link', $frontpage_id); ?>" target="_blank" class="uk-icon-button uk-icon-small uk-icon-twitter"></a>
			    </div>
			</div>
		    </div>
		</div>
	    </div>    
	    <?php
	    global $post;
	    $custom_post_id = $post->ID;
	    if (is_post_type_archive('product')) {
		$custom_post_id = get_option('woocommerce_shop_page_id');
		;
	    }
	    ?>
<?php if (get_field('display_section1', $custom_post_id) == 1): ?>
    	    <div id="header-banner" class="uk-block uk-block-collapsed uk-block-secondary">
    		<div class="page-banner"> <img src="<?php echo get_field('header_banner', $custom_post_id); ?>" alt="Home"></div>
    	    </div>
	    <?php elseif(!(is_page('contact-us'))): //elseif (get_field('display_contact_header_map') != 1): ?>
	    	 <div id="header-banner" class="uk-block uk-block-collapsed uk-block-secondary">
    		<div class="page-banner"> <img src="/wp-content/uploads/2017/04/shop-banner.jpg" alt="Home"></div>
    	    </div>
	     <?php endif; ?>

<?php if (get_field('display_contact_header_map') == 1): ?>
    	    <div id="header-banner" class="uk-block uk-block-collapsed uk-block-secondary">
    		<div class="page-banner"> 
    		    <div class="map">
    <?php echo get_field('google_map_embeded_code', $frontpage_id); ?>
    		    </div>
    		</div>
    	    </div>
<?php endif; ?>

	    <div id="header-third" class="uk-block uk-block-small uk-block-primary">
		<div class="uk-container uk-container-center">
		    <nav class="uk-navbar uk-flex uk-flex-center uk-visible-small">
			<ul class="uk-navbar-nav">
			    <li><a data-uk-offcanvas="{target:'#offcanvas-nav'}"><i class="uk-icon uk-icon-large uk-icon-bars"></i></a></li>
			</ul>
		    </nav>
			<?php if (has_nav_menu('primary')) : ?>
    		    <nav class="uk-navbar uk-flex uk-flex-center uk-hidden-small" role="navigation">
			    <?php
			    wp_nav_menu(array(
				'theme_location' => 'primary',
				'menu_class' => 'uk-navbar-nav',
			    ));
			    ?>
    		    </nav><!-- .main-navigation -->
<?php endif; ?>
		</div>
	    </div>
	</section>
