<?php
/**
 * Template Name: Workshop page
 */
?>
<?php get_header(); ?>
<?php $our_title = get_the_title(get_option('page_for_posts', true)); ?>
<div class="banner banner-secondary">
    <div class="container">
	<h1 class="title" style="text-align: center;"><?php echo $our_title; ?></h1>
    </div>
</div>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
	<div class="product-listing-wrapper wrapper wrapper-secondary wrapper-listing"> 

	    <div class="uk-block">
		<div class="uk-container uk-container-center">
		    <div class="uk-block-birds"></div>
		    <div class="main-content">

			<div class="product-listing container">

			    <div class="main-content">
				<div data-uk-grid-margin="" class="uk-grid uk-grid-width-medium-1-3">          
				    <?php
				    $args = array('post_type' => 'workshop');
				    $query = new WP_Query($args);

				    // Start the loop.
				    while ($query->have_posts()) : $query->the_post();
				    ?>


    				    <div class="th-workshop-panel">
    					<div class="uk-panel uk-panel-box uk-panel-box-primary uk-text-center">
    					    <h3 class="uk-panel-title"><?php the_title(); ?></h3>
    					    <p>
						    <?php
						    $category_detail = get_the_category($query->post->ID); //$post->ID
						    foreach ($category_detail as $cd) {
							echo $cd->cat_name . '<br>';
						    }
						    ?>
    					    </p>
    					    <p><?php the_content(); ?></p>
					    <a href="#workshop-ws001" class="get_workshop" data-uk-modal="{center:true}" data-post-id="<?php the_ID(); ?>">
						<i class="uk-icon-button uk-icon-info"></i>
					    </a>
    					</div>
    				    </div>        


					<?php
//			     End of the loop.
				    endwhile;
				    ?>
				</div>

				<!-- Modal-->        
				<div id="workshop-ws001" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
				    <div class="uk-modal-dialog uk-modal-dialog-lightbox" style="top: 121px;">
					
					<a class="uk-modal-close uk-close uk-close-alt"></a>
					<div class="workshopData">
					    
					</div>
				    </div>
				</div>      
			    </div>

			</div>

		    </div>
		</div>
	    </div>


	</div>
    </main><!-- .site-main -->

</div><!-- .content-area -->

<?php get_footer(); ?>
