<?php
/**
 * The template part for displaying content contact
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php $frontpage_id = get_option( 'page_on_front' ); ?>
<section id="contact-block">
	<div class="uk-block-contact-block uk-block-muted">
    <div data-uk-grid-match="{target:'.match-height'}" class="uk-grid">
      <div class="tm-address-block uk-width-medium-1-2 match-height" style="min-height: 402px;">
        <div class="uk-panel uk-panel-box uk-panel-box-muted match-height" style="min-height: 352px;">
          <div style="min-height: 314px;" class="uk-panel uk-panel-box uk-panel-box-muted uk-border uk-text-center uk-flex uk-flex-column uk-flex-space-around">
          <?php echo get_field('address', $frontpage_id);?>
            
            <div class="tm-address-tel uk-text-bold">Tel. <?php echo get_field('contact_no', $frontpage_id);?></div>
            <div class="tm-address-email uk-text-bold">Email: <?php echo get_field('email', $frontpage_id);?></div>
            <div class="tm-address-social">
	            <a href="<?php echo get_field('facebook_link', $frontpage_id);?>" target="_blank" class="uk-icon-button uk-icon-button-primary uk-icon-small uk-icon-facebook"> </a>
	            <a href="<?php echo get_field('twitter_link', $frontpage_id);?>" target="_blank" class="uk-icon-button uk-icon-button-primary uk-icon-small uk-icon-twitter"> </a>
	            <a href="<?php echo get_field('instaram_link', $frontpage_id);?>" target="_blank" class="uk-icon-button uk-icon-button-primary uk-icon-small uk-icon-instagram"> </a>
	            <a href="<?php echo get_field('youtube_link', $frontpage_id);?>" target="_blank" class="uk-icon-button uk-icon-button-primary uk-icon-small uk-icon-youtube"> </a>
            </div>
          </div>
        </div>
      </div>
      <div class="tm-map uk-width-medium-1-2 uk-padding-remove match-height" style="min-height: 402px;">
        <div class="uk-panel uk-panel-box-muted match-height" style="min-height: 402px;">
          <div class="map">
          <?php echo get_field('google_map_embeded_code', $frontpage_id);?></div>
        </div>
      </div>
    </div>
  </div>
</section>