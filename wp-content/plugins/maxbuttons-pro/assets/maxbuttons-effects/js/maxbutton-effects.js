
jQuery(document).ready(function(jq) {
	$ = jq;


maxEffects.prototype = {
	maxadmin: null, // reference to the main admin
	has_animation: false, // there is animation anywhere in the button
	has_effect: false, // has effect on normal
	has_hover_effect: false, // has effect on hover
	play_state: 'paused', // current play state.
	target_icon: null,
	target_icon_hover: null,
	icons: null,
	fields: null, // all fields of the admin IF
};

function maxEffects()
{

};

maxEffects.prototype.init = function()
{
	// only run in the button editor
	if ($('#new-button-form').length == 0)
		return;

	this.fields = window.maxFoundry.maxadmin.fields;

	this.maxadmin = window.maxFoundry.maxadmin;

	this.icons = window.maxFoundry.maxIcons;
	this.addPlayButton();
	this.toggleTooltip();
	this.updateIconEffect(null);

	var self = this;

	// check effects when position change for the background position block.
	$(document).on('change', '#icon_position', $.proxy(this.checkForEffects, this));

	$(document).on('mb_updated_icon', function () {  self.updateIconEffect(null) });
	// This needs a different solution.
}


// Primarely for testing if play / pause interface should be shown.
maxEffects.prototype.checkForEffects = function()
{
	var effect = $('#icon_effect').val();
	var effect_hover = $('#icon_effect_hover').val();

	// check if effects are defined here.
	if (effect !== 'none' && effect !== '')
		this.has_effect = true;

	if (effect_hover !== 'none' && effect_hover !== '')
		this.has_hover_effect = true;


	if ( (this.has_effect || this.has_hover_effect) && this.has_animation )
	{
		$('.output .inner .play_button').addClass('active');
	}
	else {
		$('.output .inner .play_button').removeClass('active');
	}

	if (this.icons.is_background)
	{
			$('#icon_effect').prop('disabled', true);
			$('#icon_effect_hover').prop('disabled', true);
	}
	else {
		$('#icon_effect').prop('disabled', false);
		$('#icon_effect_hover').prop('disabled', false);
	}

}

maxEffects.prototype.addPlayButton = function()
{
	$('.output .inner').prepend ( '<span class="play_button dashicons dashicons-controls-play"> </span>');
	$(document).on('click', '.output .inner .play_button', $.proxy(this.toggleEffects, this) ) ;

}

maxEffects.prototype.toggleTooltip = function()
{

	 var enabled = 	$('#tooltip_enable').prop('checked');
	 if (! enabled)
	 {
		 $('.output a.hover .after').remove();
		 var title = $('#link_title').val();
		 if (title.length > 1)
		 {
			 $('.output a.hover').attr('title', title);
		 }

	 		return;
	 }

	// enabled
	// check if element already there,  if not prepend
		this.fields.link_title.func = 'window.maxFoundry.maxEffects.updateTooltip';

	$after = $('.output a.hover .after');
	if (  $after.length == 0)
	{
		var title = $('#link_title').val();
		$after = $('<span class="after">' + title  + ' </span>');

		$after.css('padding', '5px 15px');
		$after.css('position', 'absolute');
		$after.css('white-space', 'nowrap');
		$after.css('border-style', 'solid');
		$after.css('line-height', '1em');
		$after.css('z-index', '5');

		$('.output a.hover').prepend($after);
		$('.output a').removeAttr('title'); // remove both titles
		//$('.output a')

		var tt_fields = $('.block_sidebar.tooltip_effects').find('input, select');
		var self = this;

		tt_fields.each(function()
		{
				var id = $(this).attr('id');
				if (typeof self.fields[id] == 'undefined')
				 	return;

				var field = self.fields[id];

				if (typeof field.css != 'undefined') // direct copy from maxbuttons-admin.js . Could be solved differently en la futuro
				{
					value = $(this).val();

					if (typeof field.css_unit != 'undefined' && value.indexOf(field.css_unit) == -1)
						value += field.css_unit;

					// a target that is checkbox but not checked should unset (empty) value.
					if ( $(this).is(':checkbox') && ! $(this).is(':checked') )
						value = '';

					self.maxadmin.putCSS(field, value, 'hover');
			  }
		});


	}

}

maxEffects.prototype.updateTooltip = function(target)
{
	$after = $('.output a.hover .after');
	val = $(target).val();

	$after.text(val);

}

maxEffects.prototype.updateIconEffect = function(event)
{
	var is_background = this.icons.is_background;
	var has_hover = this.icons.has_hover;
	var is_image = this.icons.is_image;

	var output_string = '.output .result a.normal .mb-icon';
	var output_string_hover = '.output .result a.hover .mb-icon';

	if (is_image)
	{
			output_string += ' .mb-img';
			output_string_hover += ' .mb-img';

			if (has_hover)
			{
				var output_string_hover = '.output .result a.hover .mb-icon-hover .mb-img-hover';
			}
	}
	if (this.icons.is_icon && ! is_background)
	{
		output_string += ' .svg-mbp-fa';
		output_string_hover += ' .svg-mbp-fa';

		if (has_hover)
		{
			var output_string_hover = '.output .result a.hover .mb-icon-hover .svg-mbp-fa-hover';
		}
	}

	var $output_icon = $(output_string);
	var $output_hover_icon = $(output_string_hover);

	this.target_icon = output_string;
	this.target_icon_hover = output_string_hover;

  var effect = $('#icon_effect').val();
  var effect_hover = $('#icon_effect_hover').val();

  var icon_effect_degree = $('#icon_effect_degree').val();
  var icon_effect_degree_hover = $('#icon_effect_degree_hover').val();

  var effect_transition  = $('#effect_transition').val();
	var effect_transition_hover = $('#effect_transition_hover').val();

	var start_paused = true;
	if (event !== null)
		start_paused = false;


  var args = {
      'degree' : icon_effect_degree,
      'transition': effect_transition,
			'hover': false,
			'paused' : start_paused,
  };

  var args_hover = {
      'degree':  icon_effect_degree_hover,
      'transition': effect_transition_hover,
			'hover' :  true,
			'paused' : start_paused,
  };

	this.removeEffect($output_icon);
	this.removeEffect($output_hover_icon);

  this.buildEffect(effect, $output_icon, args);
  this.buildEffect(effect_hover, $output_hover_icon, args_hover);
	this.checkForEffects();

}

maxEffects.prototype.toggleEffects = function()
{
	 var $button = $('.output .play_button'); // play button

	 if (this.has_animation && this.play_state == 'paused')
	 {
		 $(this.target_icon).css('animation-play-state','running');
		 $(this.target_icon_hover).css('animation-play-state','running');
		 $button.removeClass('dashicons-controls-play');
		 $button.addClass('dashicons-controls-pause');
		 this.play_state = 'running';
	 }
	 else {
	 	 $(this.target_icon).css('animation-play-state','paused');
		 $(this.target_icon_hover).css('animation-play-state','paused');
		 $button.addClass('dashicons-controls-play');
		 $button.removeClass('dashicons-controls-pause');
		 this.play_state = 'paused';
	 }
}

maxEffects.prototype.removeEffect = function($target)
{
	this.has_animation = false;  // reset to default before building
	this.play_state = 'paused';
	$target.css('transform', '');
	$target.css('transition', '');
	$target.css('animation', '');
	$('style#keyframes').remove();

	var $play_button = $('.output .play_button');
	$play_button.addClass('dashicons-controls-play');
	$play_button.removeClass('dashicons-controls-pause');
}

maxEffects.prototype.buildEffect = function(effect, $target, args)
{

	if ( this.icons.is_background)
	{
		this.has_animation = false;
		return false;
	}


  switch(effect)
  {
    case 'rotate':
      $target.css('transform','rotate(' + args.degree + 'deg)');
      $target.css('transition', 'all' + args.transition + 'ms ease ');
    break;
    case 'spin':
			this.has_animation = true;
      //$target.css('animation', 'mb-spin ' + args.transition + 'ms infinite linear');
			var frameName = 'mb-spin';
			if (args.hover)
				frameName += '-hover';

      var keyframes = "@keyframes " + frameName + " { 0% { transform: rotate(0); } 100% { transform: rotate(360deg); } }";
    //  $('.output .result').append('<style id="keyframes">' + keyframes + '</style>');
			this.addKeyFrame(keyframes);

      $target.css('animation', frameName + ' ' + args.transition + 'ms infinite linear');

			// reset the play / pause btw as well
			this.play_state = 'paused';
			$target.css('animation-play-state', 'paused');


			if (! args.paused)
			{
				this.toggleEffects();
			}


    break;

  }
}

maxEffects.prototype.addKeyFrame = function (keyframe)
{
	$keyframes = $('.output .result #keyframes');
	if ( $keyframes.length == 0  )
	{
		  $('.output .result').append('<style id="keyframes"></style>');
			$keyframes = $('.output .result #keyframes');
	}

	$keyframes.append(keyframe);

}


if (typeof window.maxFoundry === 'undefined')
  window.maxFoundry = {};

window.maxFoundry.maxEffects = new maxEffects();
window.maxFoundry.maxEffects.init();


}); // jquery
