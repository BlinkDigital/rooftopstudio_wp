<?php namespace MaxButtons;
defined('ABSPATH') or die('No direct access permitted');

$blockClass["icon"] = "iconEffectBlock";

// icon and image effects here. Like those funky FA rotates
class iconEffectBlock extends iconBlock
{

  static $hooks_loaded = false;

  protected $has_effect = false;
  protected $has_hover_effect = false;


  public function __construct()
  {

    $this->fields = array_merge($this->fields, array(
              'icon_effect' => array('default' => 'none'),
              'icon_effect_hover' => array('default' => 'none'),
              'icon_effect_degree' => array('default' => 0),
              'icon_effect_degree_hover' => array('default' => 0),
              'effect_transition' => array('default' => 1500),
              'effect_transition_hover' => array('default' => 1500),
    ));


    if (! self::$hooks_loaded)
    {
    //  add_action('mb/editor/afterfield/bind_to_text', array($this, 'effect_admin_fields'));
    }

    self::$hooks_loaded = true;
  }

/*  public function parse_button($domObj, , $mode = 'normal');
  {

  } */

  public function parse_css($css,  $mode = 'normal')
  {
    $css = parent::parse_css($css);
    $data = $this->data[$this->blockname];

    $button_id = $this->data['id'];

    if ($this->is_background)
      return $css;

    if (isset($data['icon_effect']) && $data['icon_effect'] !== 'none' && strlen($data['icon_effect']) > 0)
    {
      $this->has_effect = true;
      $degree = maxBlocks::getValue('icon_effect_degree');
      $transition= maxBlocks::getValue('effect_transition');

      $fx = $this->parseEffect($data['icon_effect'], array(
              'degree' => $degree,
              'transition' => $transition,
              'button_id' => $button_id,
      ));

      foreach($fx as $declr => $rule)
      {
        if ($this->is_image)
          $css['mb-img']['normal'][$declr] = $rule;
        else
          $css['svg-mbp-fa']['normal'][$declr] = $rule; // THIS here need a rule for background icons
      }
    }

    if (isset($data['icon_effect_hover']) && $data['icon_effect_hover'] !== 'none' && strlen($data['icon_effect_hover']) > 0)
    {
      $this->has_hover_effect = true;

      $degree = maxBlocks::getValue('icon_effect_degree_hover');
      $transition= maxBlocks::getValue('effect_transition_hover');

      $fx = $this->parseEffect($data['icon_effect_hover'], array(
          'degree' => $degree,
          'transition' => $transition,
          'button_id' => $button_id . '-hover',
        ));

      foreach($fx as $declr => $rule)
      {
          /// *** Not sure if this is correct. Might need a hard look
        if ($this->is_image && $this->has_hover && ! $this->is_background)
          $css['mb-img-hover']['normal'][$declr] = $rule;
        elseif($this->is_image)
          $css['mb-img']['hover'][$declr] = $rule;
        elseif ($this->is_icon && $this->has_hover && ! $this->is_background)
        {

          $css['svg-mbp-fa-hover']['normal'][$declr] = $rule;
          $css['svg-mbp-fa-hover']['hover'][$declr] = $rule;
        }
        else {
          $css['svg-mbp-fa']['hover'][$declr] = $rule;
        }

      }
    }
    return $css;
  }

  protected function parseEffect($effect, $args)
  {
    $fx = array();

    $degree = isset($args['degree']) ? $args['degree'] : false;
    $transition = isset($args['transition']) ? $args['transition'] : false;
    $button_id = $args['button_id']; // must-have

    switch($effect)
    {
      case 'rotate':
        $fx['transform'] = 'rotate(' . $degree . 'deg)';
        $fx['transition'] = 'all ' .  $transition . 'ms ease-in-out ';
        $fx['animation'] = 'none';
      break;
      case 'spin':
          $fx['animation'] = 'mb-spin-' . $button_id . ' ' . $transition . 'ms infinite linear';
          $fx['keyframes-name'] = 'mb-spin-'. $button_id;
          $degree = 360; /// ???
          $fx['keyframes']  = ' 0% { transform: rotate(0); } 100% { transform: rotate(' . $degree . 'deg); }';
      break;
    }

    return $fx;
  }

  public function map_fields($map)
  {
      $map = parent::map_fields($map);

      $map['icon_effect']["func"] = "window.maxFoundry.maxEffects.updateIconEffect";
      $map['icon_effect_hover']["func"] = "window.maxFoundry.maxEffects.updateIconEffect";
      $map['icon_effect_degree']["func"] = "window.maxFoundry.maxEffects.updateIconEffect";
      $map['icon_effect_degree_hover']["func"] = "window.maxFoundry.maxEffects.updateIconEffect";
      $map['effect_transition']["func"] = "window.maxFoundry.maxEffects.updateIconEffect";
      $map['effect_transition_hover']["func"] = "window.maxFoundry.maxEffects.updateIconEffect";


      return $map;
  }

  public function sidebar()
  {

    $data = $this->data[$this->blockname];
    $admin = MB()->getClass('admin');

    $startbar = new maxField('sidebar_start');
    $startbar->title = __('Icon and Image Effects', 'maxbuttons-pro');
    $startbar->label = __('Effects', 'maxbuttons-pro');
    $admin->addField($startbar);

  /*  $rotate = new maxField('switch');
    $rotate->id = 'rotate_switch';
    $rotate->name = $rotate->id;
    $rotate->value = 1;
    $admin->addField($rotate, 'start', 'end');
*/

  $this->effect_fields();

  $heading = new maxField('spacer');
  $heading->label = __("Hover", 'maxbuttons-pro');
  $heading->name = 'hover_label';
  $admin->addField($heading, 'start', 'end');

  $this->effect_fields('hover');


/*    $effect = new maxField('option_select');
    $effect->id = 'icon_effect';
    $effect->name = $effect->id;
    $effect->label = __('Effect' ,'maxbuttons-pro');
    $effect->options = $effects;
    $effect->selected =  maxBlocks::getValue($effect->id);
    $admin->addField($effect, 'start', ''); */

  /*  $effect_hover = clone $effect;
    $effect_hover->id = 'icon_effect_hover';
    $effect_hover->label = __('Hover', 'maxbuttons-pro');
    $effect_hover->name = $effect_hover->id;
    $effect_hover->selected = maxBlocks::getValue($effect_hover->id);

    $admin->addField($effect_hover, '', 'end');
*/
  /*  $effect_degree = new maxField('slider');
    $effect_degree->id = 'icon_effect_degree';
    $effect_degree->name = $effect_degree->id;
    $effect_degree->min = -180;
    $effect_degree->max = 180;
    $effect_degree->value =  maxBlocks::getValue($effect_degree->id);
    $effect_degree->label = __('Rotation Degree', 'maxbuttons-pro');
    $effect_degree->conditional = htmlentities(json_encode(array('target' => 'icon_effect', 'values' => 'rotate')));

    $admin->addField($effect_degree, 'start', ''); */

/*    $effect_degree_hover = clone($effect_degree);
    $effect_degree_hover->id = 'icon_effect_degree_hover';
    $effect_degree_hover->name = $effect_degree_hover->id;
    $effect_degree_hover->value = maxBlocks::getValue($effect_degree_hover->id);
    $effect_degree_hover->label = __('Hover', 'maxbuttons-pro');
    $effect_degree->conditional = htmlentities(json_encode(array('target' => 'icon_effect_hover', 'values' => 'rotate')));

    $admin->addField($effect_degree_hover, '', 'end'); */

/*    $transition = new maxField('number');
    $transition->id = 'effect_transition';
    $transition->name = $transition->id;
    $transition->min = 0;
    $transition->value = maxBlocks::getValue($transition->id);
    $transition->label = __('Effect Duration (in MS)');
    $transition->help = __('1 Second is 1000 ms', 'maxbuttons-pro');

    $admin->addField($transition, 'start', 'end'); */


    $sidebarend = new maxField('sidebar_end');

    $admin->addField($sidebarend);
  }

  public function effect_fields($hover = false)
  {

    $data = $this->data[$this->blockname];
    $admin = MB()->getClass('admin');

    $posfix = ($hover) ? '_hover' : '';

    $conditional = '';
    $effects = array(
      'none'   => __('None', 'maxbuttons-pro'),
      'rotate' => __('Rotate', 'maxbuttons-pro'),
      'spin' => __('Spin', 'maxbuttons-pro'),
  //    'pulse' => __('Pulse', 'maxbuttons-pro'),
    );

    $effect = new maxField('option_select');
    $effect->id = 'icon_effect' . $posfix;
    $effect->name = $effect->id;
    $effect->label = __('Effect' ,'maxbuttons-pro');
    $effect->options = $effects;
  //  $effect->start_conditional = htmlentities(json_encode(array('target' => 'icon_position', 'values' => array('left','right', 'top', 'bottom') )));
    $effect->selected =  maxBlocks::getValue($effect->id);

    $admin->addField($effect, 'start', 'end');

    $fx_note = new maxField('generic');
    $fx_note->name = 'effect_note' . $posfix;
    $fx_note->id = $fx_note->name;
    $fx_note->content = '<p class="error note">' . __('Background Icons and Images can\'t contain effects. This is a technical limitation. ', 'maxbuttons-pro') .  '</p>';
    $fx_note->start_conditional = htmlentities(json_encode(array('target' => 'icon_position', 'values' => array('background') )));

    $admin->addField($fx_note, 'start','end');

    $effect_degree = new maxField('slider');
    $effect_degree->id = 'icon_effect_degree' . $posfix;
    $effect_degree->name = $effect_degree->id;
    $effect_degree->min = -180;
    $effect_degree->max = 180;
    $effect_degree->value =  maxBlocks::getValue($effect_degree->id);
    $effect_degree->label = __('Rotation Degree', 'maxbuttons-pro');
    $effect_degree->start_conditional = htmlentities(json_encode(array('target' => 'icon_effect' . $posfix, 'values' => 'rotate')));
    $admin->addField($effect_degree, 'start', 'end');

    /*$fxd = new maxField('generic');
    $fxd->name = 'datalist';
    $fxd->content = "<datalist id='icon_effect_degree$postfix'> <option id='90'> <option id='45'> <option id='135'> </datalist>";
    $admin->addField($fxd); */

    $transition = new maxField('number');
    $transition->id = 'effect_transition' . $posfix;
    $transition->name = $transition->id;
    $transition->min = 0;
    $transition->value = maxBlocks::getValue($transition->id);
    $transition->label = __('Duration / Speed (in MS)');
    $transition->help = __('1 Second is 1000 ms', 'maxbuttons-pro');
    $transition->start_conditional = htmlentities(json_encode(array('target' => 'icon_effect' . $posfix, 'values' => array('rotate', 'spin') )));

    $admin->addField($transition, 'start', 'end');


  }

}
