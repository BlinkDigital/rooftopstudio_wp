<?php namespace MaxButtons;
defined('ABSPATH') or die('No direct access permitted');

$blockClass["basic"] = "hoverBlock";
//$blockOrder[0][] = "basic";

// Hover EFFECTS when switching from normal to hover.
// https://fontawesome.com/how-to-use/on-the-web/styling/power-transforms
class hoverBlock extends basicBlockPRO
{

  public function __construct()
  {

    $this->fields['tooltip_enable'] =  array(
                            'default' => 0,
    );

    $this->fields['tooltip_color'] = array(
                            'default' => '#000',
                            'css' => 'color',
                            'csspseudo' => 'after'
          );
     $this->fields['tooltip_color_background'] = array (
                            'default' => '#FFFFE1',
                            'css' => 'background-color',
                            'csspseudo' => 'after',
     );

     $this->fields['tooltip_border_width'] = array('default' => '1px',
                            'css' => 'border-width',
                            'csspseudo' => 'after',
      );
      $this->fields['tooltip_color_border'] = array (
                             'default' => '#000000',
                             'css' => 'border-color',
                             'csspseudo' => 'after',
      );

      $this->fields['tooltip_border_radius'] = array(
                            'default' => '1px',
                            'css' => 'border-radius',
                            'csspseudo' => 'after',
      );

      $this->fields['tooltip_font'] = array(
                            'default' => 'Arial',
                            'css' => 'font-family',
                            'csspseudo' => 'after',
      );

      $this->fields['tooltip_font_size'] = array(
                            'default' => '12px',
                            'css' => 'font-size',
                            'csspseudo' => 'after',
      );

      $this->fields['tooltip_padding_top']  = array(
                             'default' => '5px',
                             'css' => 'padding-top',
                             'csspseudo' => 'after',
                           );
     $this->fields['tooltip_padding_bottom']  = array(
                            'default' => '5px',
                            'css' => 'padding-bottom',
                            'csspseudo' => 'after',
                          );
     $this->fields['tooltip_padding_left']  = array(
                           'default' => '15px',
                           'css' => 'padding-left',
                           'csspseudo' => 'after',
                         );
      $this->fields['tooltip_padding_right']  = array(
                              'default' => '15px',
                              'css' => 'padding-right',
                              'csspseudo' => 'after',
                            );


      $this->fields['tooltip_top'] = array(
                            'default' => '-25%',
                            'css' => 'top',
                            'csspseudo' => 'after',
      );

      $this->fields['tooltip_left'] = array(
                            'default' => '75%',
                            'css' => 'left',
                            'csspseudo' => 'after',
      );

      parent::__construct();

  }

   public function parse_button($domObj, $mode = 'normal')
   {
     $domObj = parent::parse_button($domObj, $mode);

     /*if ($mode == 'editor')
     {
        $data = $this->data[$this->blockname];
        var_dump(maxBlocks::getValue('tooltip_enable'));
        if (maxBlocks::getValue('tooltip_enable') == 1)
        {
          echo "HOBO";
              $hover = $domObj->find('a.hover',0);
              var_dump($hover);
        }
     } */

     if (maxBlocks::getValue('tooltip_enable') == 1)
     {
           $btn = $domObj->find('a',0);
           $btn->tooltip = $btn->title;
           unset($btn->title);
     }


     return $domObj;

   }

   public function parse_css($css,  $mode = 'normal')
   {
     $data = $this->data[$this->blockname];
     $css =  parent::parse_css($css, $mode);

     if (maxBlocks::getValue('tooltip_enable') != 1)
     {
       return $css;
     }

     $pseudo = 'after';
     $element = 'maxbutton';

     if ($mode == 'editor') // editor preview is a different element.
     {
      $pseudo = 'hover';
      $element = 'after';

      $hover_after =  array();
      if (isset($css['maxbutton']['hover:after']))
      {
        $hover_after = $css['maxbutton']['hover:after'];
        $css[$element][$pseudo] = $hover_after;
        unset($css['maxbutton']['hover:after']);
      }

     }

     // these won't work in button editor, since after element is created via Javascript
      $css[$element][$pseudo]['content'] = 'attr(tooltip)';
    //  $css[$element][$pseudo]['padding'] = '5px 15px';
      $css[$element][$pseudo]['position'] = 'absolute';
      $css[$element][$pseudo]['white-space'] = 'nowrap';
      $css[$element][$pseudo]['line-height'] = '1em';
      $css[$element][$pseudo]['z-index'] = '-1';
      $css[$element]['hover:after']['opacity'] = '1';
      $css[$element]['hover:after']['z-index'] = '5';

      $css[$element][$pseudo]['transition'] = 'all 500ms linear ';
      $css[$element]['after']['content'] = 'attr(tooltip)';
      $css[$element]['after']['opacity'] = 0;

      $css[$element][$pseudo]['border-style'] = 'solid';

      if ($data['tooltip_border_width'] > 0)
      {
//        echo ' boeteboeteoboeh';
      //  $css[$element]['after']['border-style'] = 'solid';
      }
    //  $css[$element]['hover:before']['content'] = '"hello"';
    //  $css[$element]['hover:before']['opacity'] = '1';

//echo "<PRE>"; print_R($css['after']); echo "</PRE>";
//      $css['maxbutton']['hover:after']['left']  = '20%';
     return $css;

   }

   public function map_fields($map)
   {
     $func = 'window.maxFoundry.maxEffects.toggleTooltip';

     $map = parent::map_fields($map);
    /* ['tooltip_color', 'tooltip_color_background', 'tooltip_border_with',
    'tooltip_border_radius', 'tooltip_font','tooltip_font_size', 'tooltip_top', 'tooltip_left'];*/

    $map['tooltip_enable']['func'] = $func;
    $map['tooltip_font']['func'] = 'updateFont';

    $csspart = 'after';
    $map['tooltip_color']['csspart'] = 'after';
    $map['tooltip_color_background']['csspart'] = 'after';
    $map['tooltip_border_width']['csspart'] = 'after';
    $map['tooltip_color_border']['csspart'] = 'after';
    $map['tooltip_border_radius']['csspart'] = 'after';
    $map['tooltip_font']['csspart'] = 'after';
    $map['tooltip_font_size']['csspart'] = 'after';
    $map['tooltip_top']['csspart'] = 'after';
    $map['tooltip_left']['csspart'] = 'after';
    $map['tooltip_padding_top']['csspart'] = 'after';
    $map['tooltip_padding_bottom']['csspart'] = 'after';
    $map['tooltip_padding_left']['csspart'] = 'after';
    $map['tooltip_padding_right']['csspart'] = 'after';

/*     $map['tooltip_color']['func'] = $func;
     $map['tooltip_color_background']['func'] = $func;
     $map['tooltip_border_width']['func'] = $func;
     $map['tooltip_border_radius']['func'] = $func;
     $map['tooltip_font']['func'] = $func;
     $map['tooltip_font_size']['func'] = $func;
*/

     return $map;

   }

    public function sidebar()
    {
        $data = $this->data[$this->blockname];
        $admin = MB()->getClass('admin');
        $icon_url = MB()->get_plugin_url() . 'images/icons/';

        $startbar = new maxField('sidebar_start');
        $startbar->title = __('Tooltip', 'maxbuttons-pro');
        $startbar->label = __('Advanced Tooltip', 'maxbuttons-pro');
        $startbar->name = 'tooltip_effects';
        $admin->addField($startbar);

        $enable = new maxField('switch');
        $enable->id = 'tooltip_enable';
        $enable->name = $enable->id;
        $enable->label = __('Enable Advanced Tooltip','maxbuttons-pro');
        $enable->value = 1;
        $enable->checked = checked( maxBlocks::getValue('tooltip_enable'), 1, false);
        $admin->addField($enable, 'start', 'end');

  /*      $field_title = new maxField();
        $field_title->label = __('Button Tooltip', 'maxbuttons');
        $field_title->name = 'link_title';  // title is too generic
        $field_title->id = 'link_title_effect';
        $field_title->value =  maxBlocks::getValue('link_title');
        $field_title->help = __('<p>This text will appear when hovering over the button</p>
        <p class="shortcode">Shortcode attribute : linktitle</p>', 'maxbuttons');
        $admin->addField($field_title, 'start', 'end');
  */

        $tt_color = new maxField('color');
        $tt_color->id = 'tooltip_color';
        $tt_color->name = $tt_color->id;
        $tt_color->label = __('Tooltip Color', 'maxbuttons-pro');
        $tt_color->value= maxBlocks::getColorValue($tt_color->id);
        $admin->addField($tt_color, 'start', '');

        $tt_color_bg = new maxField('color');
        $tt_color_bg->id = 'tooltip_color_background';
        $tt_color_bg->name = $tt_color_bg->id;
        $tt_color_bg->label = __('Tooltip background');
        $tt_color_bg->value = maxBlocks::getColorValue($tt_color_bg->id);
        $admin->addField($tt_color_bg, '', 'end');

        $tt_border = new maxField('number');
        $tt_border->id = 'tooltip_border_width';
        $tt_border->name = $tt_border->id;
        $tt_border->label = __('Tooltip Border', 'maxbuttons-pro');
        $tt_border->value = maxUtils::strip_px(maxBlocks::getValue($tt_border->id));
        $tt_border->min = 0;
        $tt_border->inputclass = 'tiny';
        $tt_border->after_input = __('px', 'maxbuttons-pro');
        $admin->addField($tt_border, 'start', 'end');

        $tt_color_border = new maxField('color');
        $tt_color_border->id = 'tooltip_color_border';
        $tt_color_border->name = $tt_color_border->id;
        $tt_color_border->label = __('Border Color', 'maxbuttons-pro');
        $tt_color_border->value = maxBlocks::getColorValue($tt_color_border->id);
        $admin->addField($tt_color_border, 'start', 'end');

        $tt_radius = new maxField('number');
        $tt_radius->id = 'tooltip_border_radius';
        $tt_radius->name = $tt_radius->id;
        $tt_radius->inputclass = 'tiny';
        $tt_radius->label = __('Border Radius', 'maxbuttons-pro');
        $tt_radius->value = maxUtils::strip_px(maxBlocks::getValue($tt_radius->id));
        $tt_radius->min = 0;
        $tt_radius->after_input = __('px', 'maxbuttons-pro');
        $admin->addField($tt_radius, 'start', 'end');

        $fonts = MB()->getClass('admin')->loadFonts();

        $tt_font = new maxField('option_select');
        $tt_font->label = __('Font','maxbuttons');
        $tt_font->name = 'tooltip_font';
        $tt_font->id = $tt_font->name;
        $tt_font->selected = maxBlocks::getValue($tt_font->id);
        $tt_font->options = $fonts;
        $admin->addField($tt_font,'start');


        $tt_fsize = new maxField('number');
        $tt_fsize->name = 'tooltip_font_size';
        $tt_fsize->id= $tt_fsize->name;
        $tt_fsize->inputclass = 'tiny';
        $tt_fsize->min = 8;
        $tt_fsize->after_input = __('px', 'maxbuttons-pro');
      //  $tt_fsize->label = __('Font Size', 'maxbuttons-pro');
        $tt_fsize->value = maxUtils::strip_px(maxBlocks::getValue($tt_fsize->id));
        $admin->addField($tt_fsize, '', 'end');

        // Padding - trouble
        $ptop = new maxField('number');
        $ptop->label = __('Padding', 'maxbuttons');
        $ptop->id = 'tooltip_padding_top';
        $ptop->name = $ptop->id;
        $ptop->min = 0;
        $ptop->inputclass = 'tiny';
        $ptop->before_input = '<img src="' . $icon_url . 'p_top.png" title="' . __("Padding Top","maxbuttons") . '" >';
        $ptop->value = maxUtils::strip_px(maxBlocks::getValue($ptop->id));
        $admin->addField($ptop,'start');

        $pright = new maxField('number');
        $pright->id = 'tooltip_padding_right';
        $pright->name = $pright->id;
        $pright->min = 0;
        $pright->inputclass = 'tiny';
        $pright->before_input = '<img src="' . $icon_url . 'p_right.png" class="icon padding" title="' . __("Padding Right","maxbuttons") . '" >';
        $pright->value = maxUtils::strip_px(maxBlocks::getValue($pright->id));
        $admin->addField($pright);

        $pbottom = new maxField('number');
        $pbottom->id = 'tooltip_padding_bottom';
        $pbottom->name = $pbottom->id;
        $pbottom->min = 0;
        $pbottom->inputclass = 'tiny';
        $pbottom->before_input = '<img src="' . $icon_url . 'p_bottom.png" class="icon padding" title="' . __("Padding Bottom","maxbuttons") . '" >';
        $pbottom->value = maxUtils::strip_px(maxBlocks::getValue($pbottom->id));
        $admin->addField($pbottom);

        $pleft = new maxField('number');
        $pleft->id = 'tooltip_padding_left';
        $pleft->name = $pleft->id;
        $pleft->min = 0;
        $pleft->inputclass = 'tiny';
        $pleft->before_input = '<img src="' . $icon_url . 'p_left.png" class="icon padding" title="' . __("Padding Left","maxbuttons") . '" >';
        $pleft->value = maxUtils::strip_px(maxBlocks::getValue($pleft->id));
        $admin->addField($pleft,'', 'end');

        // positioning
        $tt_left = new maxField('number');
        $tt_left->name = 'tooltip_left';
        $tt_left->id = $tt_left->name;
        $tt_left->label = __('Position Left', 'maxbuttons-pro');
        $tt_left->inputclass = 'tiny';
        $tt_left->after_input = '%';
        $tt_left->value = maxUtils::strip_px(maxBlocks::getValue($tt_left->id));
        $admin->addField($tt_left, 'start');

        $tt_top = new maxField('number');
        $tt_top->id = 'tooltip_top';
        $tt_top->name = $tt_top->id;
        $tt_top->label = __('Position Top', 'maxbuttons-pro');
        $tt_top->value = maxUtils::strip_px(maxBlocks::getValue($tt_top->id));
        $tt_top->inputclass = 'tiny';
        $tt_top->after_input = '%';
        $tt_top->help = __('Position in percentage. 0 is centered, negative numbers offset to left / top, positive to right / bottom', 'maxbuttons-pro');
        $admin->addField($tt_top, '', 'end');

        $tt_trans = new maxField('number');

        $sidebarend = new maxField('sidebar_end');
        $admin->addField($sidebarend);

    }
} // class
