<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<section id="footer">
  <div class="uk-block uk-block-default">
    <div class="uk-container uk-container-center uk-link-reset uk-text-blue">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-3 uk-margin-bottom">
          <div class="copyright uk-text-center"><p>&copy; 2016 <a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name');?></a></p></div>
        </div>
        <?php if (has_nav_menu('footer')): ?>
		    	<div class="uk-width-1-1 uk-width-medium-2-3">
					<?php
						wp_nav_menu(array(
						    'theme_location' => 'footer',
						    'container'      => 'ul',
						    'menu_class'     => 'uk-subnav footer-links uk-flex-center',
						));
					?>
				</div><!-- .footer-navigation -->
	    <?php endif;?>
      </div>
    </div>
  </div>
</section>

<div id="offcanvas-nav" class="uk-offcanvas" aria-hidden="true">
  <div class="uk-offcanvas-bar">

	<?php
		wp_nav_menu(array(
		    'theme_location' => 'responsive-nav',
		    'container'      => 'ul',
		    'menu_class'     => 'uk-nav uk-nav-offcanvas uk-nav-parent-icon',
		));
	?>

    <!-- <ul data-uk-nav="" class="uk-nav uk-nav-offcanvas uk-nav-parent-icon">
      <li><a href="/" class="active">Home</a></li>
      <li><a href="/pages/blocks">Blocks</a></li>
      <li><a href="/pages/classes">Classes</a></li>
      <li><a href="/pages/shows">Shows</a></li>
      <li><a href="/pages/studio-hire">Studio Hire</a></li>
      <li><a href="/pages/parties">Parties</a></li>
      <li><a href="/pages/workshops">Workshops</a></li>
      <li class="uk-parent uk-open" aria-expanded="true"><a href="#">Shop</a>
        <div style="overflow: hidden; position: relative;" class=""><ul class="uk-nav-sub">
          <li><a href="/pages/shop">Browse Shop</a></li>
          <li><a href="/pages/checkout">Checkout</a></li>
        </ul></div>
      </li>
      <li><a href="/pages/uniform">Uniform</a></li>
      <li><a href="/pages/contact-us">Contact</a></li>
    </ul> -->
  </div>
</div>

<?php wp_footer();?>
</body>
</html>
