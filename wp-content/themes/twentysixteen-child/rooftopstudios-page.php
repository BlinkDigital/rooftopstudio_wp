<?php
/**
 *Template Name: RoofTopStudios page
 */
?>
<?php get_header(); ?>

<section id="content">
<?php if ( have_posts() ) : ?>

	<?php
	// Start the loop.
	while ( have_posts() ) : the_post();

		/*
		 * Include the Post-Format-specific template for the content.
		 * If you want to override this in a child theme, then include a file
		 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
		 */
		get_template_part( 'template-parts/content', get_post_format() );

	// End the loop.
	endwhile;

// If no content, include the "No posts found" template.
else :
	get_template_part( 'template-parts/content', 'none' );

endif;
?>
</section>

<?php if(get_field('display_section_contact2') == 1): ?>

	<?php get_template_part( 'template-parts/content', 'contact2' ); ?>

<?php endif; ?>

<?php if(get_field('display_section3') == 1): ?>
  
    <?php get_template_part( 'template-parts/content', 'contact' ); ?>
  
<?php endif; ?>

<?php get_footer(); ?>
