<?php

namespace MaxButtons;
defined('ABSPATH') or die('No direct access permitted');


class maxButtonsEffects
{
   protected static $instance;
   protected $debug_mode = false;

    public function __construct()
    {
      add_action('admin_enqueue_scripts', array($this,'add_admin_scripts'),20);

      if ( defined('MAXBUTTONS_DEBUG') && MAXBUTTONS_DEBUG)
   			$this->debug_mode = true;

 		   self::$instance = $this;
    }

    public static function getInstance()
  	{
  		return self::$instance;
  	}

    public function add_admin_scripts()
    {
      $version = MAXBUTTONS_FX_VERSION;

      $url = trailingslashit(plugin_dir_url(__FILE__));
      $js_url =  $url . 'js/';
      if (! $this->debug_mode)
        $js_url .= 'min/';

      wp_register_script('maxbuttons-effects', $js_url . 'maxbutton-effects.js', array('jquery'), $version, true);

      wp_enqueue_script('maxbuttons-effects');

      wp_register_style('maxbuttons-style', $url .'css/maxbuttons-effects.css', array(), $version);
      wp_enqueue_style('maxbuttons-style');
    }

}
