<?php get_header(); ?>
<?php $_SESSION['workshopurl'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>
<style>
    .stripe-button-el {
        display: none !important;
    }
</style>
<?php while (have_posts()) : the_post();
    ?>
    <?php // $our_title = get_the_title(get_option('page_for_posts', true));?>
    <section id="content">
        <div class="uk-block uk-block-muted">
            <div class="uk-container uk-container-center">
                <div class="believe uk-text-center">
                    <h3><?php the_title(); ?></h3>
                    <!--<p> Workshop Date: <?php /*the_field('workshop_date', get_the_ID()); */ ?></p>-->
                </div>
            </div>
        </div>


        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div class="product-listing-wrapper wrapper wrapper-secondary wrapper-listing">
                    <div id="temp"></div>
                    <div class="uk-block">
                        <div class="uk-container uk-container-center">
                            <div class="uk-block-birds"></div>
                            <div class="main-content uk-grid">
                                <div class="uk-width-1-1 uk-form">
                                    <form id="workshop-frm" method="post">
                                        <fieldset>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Child Name</label>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="price" value="0" id="total_price">
                                                <!-- <input type="hidden" name="workshop_date"
                                                       value="<?php /*the_field('workshop_date', get_the_ID()); */ ?>"
                                                       id="workshop_date">-->
                                                <input type="hidden" name="workshop_name" value="<?php the_title(); ?>"
                                                       id="workshop_name">
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="child_name" id="child_name"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Parent/Guardian Name</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="parent_name" id="parent_name"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Childs Age</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="child_age" id="child_age"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Childs D.O.B</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="date" name="child_dob" id="child_dob"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Address</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <textarea name="child_address" id="child_address"
                                                              class="input uk-width-1-1" required></textarea>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Email Address</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="child_email" id="child_email"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Emergency Contact Name 1</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="emergncy_name1" id="emergncy_name1"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Emergency Contact Name 2</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="emergncy_name2" id="emergncy_name2"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Emergency Tel Number 1</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="emergncy_contact1" id="emergncy_contact1"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>

                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Emergency Tel Number 2</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="emergncy_contact2" id="emergncy_contact2"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Any Medical Conditions Or Anything Else You think We
                                                            Should Be Aware Of:</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="text" name="medical_condtn" id="medical_condtn"
                                                           class="input uk-width-1-1" required>
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>I Consent To The Use Of Face Paints (please tick)</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="checkbox" name="face_paints" id="face_paints"
                                                           value="I Consent To The Use Of Face Paints"
                                                           class="input uk-width-1-1">
                                                </div>
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>I Consent To RT Studios Taking Photos/Videos During The
                                                            Workshop For Promo Use. (please tick)</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <input type="checkbox" name="photos" value="Photos/Videos"
                                                           id="photos" class="input uk-width-1-1">
                                                </div>
                                            </div>
                                            <?php while (has_sub_field("workshop_property", get_the_ID())): ?>
                                                <?php $price = get_sub_field('workshop_price'); ?>
                                                <?php $time = get_sub_field('workshop_time'); ?>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-6">
                                                        <div class="input">
                                                            <label><strong><?php echo $time; ?> Booking</strong><br>
                                                                Please state which <?php echo $time; ?> you
                                                                require.</label>
                                                        </div>
                                                    </div>
                                                    <div class="uk-width-medium-3-4">
                                                        <label><input type="radio" name="day_booking"
                                                                      value="<?php echo $price; ?>" id="day_booking"
                                                                      class="input uk-width-1-1 day_booking" required>
                                                            £<?php echo $price; ?></label>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>

                                            <div class="uk-grid">
                                                <div>
                                                    <div class="input">
                                                        <label><strong>Please email <a href="mailto:info@rooftopstudios.co.uk">info@rooftopstudios.co.uk</a> to discuss your early drop off and late pick ups.</strong></label>
                                                    </div>
                                                </div>
<!--                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label><strong>Do you require early drop off or late pick
                                                                up?</strong><br> If so, please state which days and
                                                            times (£5.50 per hour)</label>
                                                    </div>
                                                </div>-->
<!--                                                <div class="uk-width-medium-3-4">
                                                    <label><input type="checkbox" value="5.50" name="early_drop"
                                                                  id="early_drop" class="input uk-width-1-1"> Early Drop</label>
                                                    <br>
                                                    <label><input type="checkbox" value="5.50" name="late_pickup"
                                                                  id="late_pickup" class="input uk-width-1-1"> Late Pick
                                                        Up</label>

                                                </div>-->
                                            </div>
                                            <div class="uk-grid">
                                                <div class="uk-width-medium-1-6">
                                                    <div class="input">
                                                        <label>Workshop Date</label>
                                                    </div>
                                                </div>
                                                <div class="uk-width-medium-3-4">
                                                    <?php $values = get_field('available_dates');
                                                    if ($values):
                                                        foreach ($values as $item):
                                                            ?>
                                                            <label><input type="radio" name="workshop_date"
                                                                          value="<?php echo $item['date']; ?>"
                                                                          id="workshop_date"
                                                                          class="input uk-width-1-1 workshop_date">
                                                                <?php echo $item['date']; ?></label>
                                                            <br/>
                                                        <?php
                                                        endforeach;
                                                        if (count($values) > 1):
                                                            ?>
                                                            <label><input type="radio" name="workshop_date"
                                                                          value="<?php echo 'Full ' . count($values) . ' days'; ?>"
                                                                          id="workshop_date"
                                                                          class="input uk-width-1-1 workshop_date">
                                                                <?php echo 'Full ' . count($values) . ' days'; ?>
                                                            </label>
                                                        <?php
                                                        endif;
                                                    endif;
                                                    ?>
                                                </div>
                                            </div>
                                            <p>
                                                <button type="submit" name="workshop_submit" id="workshop_booking"
                                                        class="submit_frm uk-button turquoise uk-width-1-1">Submit
                                                </button>
                                            </p>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="uk-width-1-3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main><!-- .site-main -->
        </div><!-- .content-area -->
    </section>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>

<script>

    jQuery('#workshop_booking').click(function () {
        if (jQuery('iframe').length > 0) {
            for (var i = 0; i <= jQuery('iframe').length; i++) {
                if (jQuery('iframe')[i].name == "stripe_checkout_app" || jQuery('iframe') != undefined) {
                    jQuery('iframe')[i].remove();
                }
            }
        }

    });
</script>
