<?php
namespace MaxButtons;

define('MAXBUTTONS_FX_VERSION', '1.0');

require_once('class-effect.php');


add_action('plugins_loaded', function () {

  if ( ! defined('MAXBUTTONS_PRO_ROOT_FILE') )
    return; // no pro no effects.

  add_filter('mb-block-paths', maxUtils::namespaceit('fx_blocks'));

  function fx_blocks($paths)
  {


    $fx_path = trailingslashit(plugin_dir_path(__FILE__)) . 'blocks';
    $paths[] = $fx_path;
    return $paths;
  }

  function MBFX()
  {
      return maxButtonsEffects::getInstance();
  }
  $m = new maxButtonsEffects();

});
