<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


get_header(); 
?>

	<section id="content">

		<div class="uk-block uk-block-muted">

			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

					<div class="uk-container uk-container-center uk-text-center">

						<h3><?php the_title();?></h3>

						<?php the_excerpt();?>

					</div>

			<?php endif; ?>

			<?php
				/**
				 * woocommerce_archive_description hook.
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				do_action( 'woocommerce_archive_description' );
			?>
			
		</div>
		<div class="uk-block">
			
			<div class="uk-container uk-container-center">

				<div class="uk-block-birds"></div>


							<?php
									/**
									 * woocommerce_before_main_content hook.
									 *
									 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
									 * @hooked woocommerce_breadcrumb - 20
									 * @hooked WC_Structured_Data::generate_website_data() - 30
									 */
									do_action( 'woocommerce_before_main_content' );
								?>
						

							<?php if ( have_posts() ) : ?>

								
								<div class="product-tabs-navigation uk-width-medium-1-4">
									<h6>Categories</h6>
									<ul id="my-id">
										<?php woocommerce_product_subcategories(); ?> 
									</ul>
								</div>
								<div class="product-tabs-content uk-width-medium-3-4">
								<?php
									/**
									 * woocommerce_before_shop_loop hook.
									 *
									 * @hooked woocommerce_result_count - 20
									 * @hooked woocommerce_catalog_ordering - 30
									 */
									do_action( 'woocommerce_before_shop_loop' );
								?>
								<?php woocommerce_product_loop_start(); ?>

									<?php while ( have_posts() ) : the_post(); ?>

										<?php
											/**
											 * woocommerce_shop_loop hook.
											 *
											 * @hooked WC_Structured_Data::generate_product_data() - 10
											 */
											do_action( 'woocommerce_shop_loop' );
										?>

										<?php wc_get_template_part( 'content', 'product' ); ?>

									<?php  endwhile; // end of the loop. 
									wp_reset_postdata(); ?>

								<?php woocommerce_product_loop_end(); ?>

								<?php
									/**
									 * woocommerce_after_shop_loop hook.
									 *
									 * @hooked woocommerce_pagination - 10
									 */
									do_action( 'woocommerce_after_shop_loop' );
								?>
								</div>

							<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

								<?php
									/**
									 * woocommerce_no_products_found hook.
									 *
									 * @hooked wc_no_products_found - 10
									 */
									do_action( 'woocommerce_no_products_found' );
								?>

							<?php endif; ?>

						<?php
							/**
							 * woocommerce_after_main_content hook.
							 *
							 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
							 */
							do_action( 'woocommerce_after_main_content' );
						?>

						<?php
							/**
							 * woocommerce_sidebar hook.
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							do_action( 'woocommerce_sidebar' );
						?>
			</div>
		</div>
	</section>

 <?php
 global $post;

 $custom_post_id = $post->ID;

 if(is_archive('product')){

    $custom_post_id = 124;

 }

if(get_field('display_section3',$custom_post_id) == 1): ?>
  
    <?php get_template_part( 'template-parts/content', 'contact' ); ?>
  
<?php endif; ?>

<?php get_footer(); ?>
