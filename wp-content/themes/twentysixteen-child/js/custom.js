jQuery(function($) {
    function get_workshop_data() {
        $('.get_workshop').click(function() {
            var postid = $(this).attr('data-post-id');
            $.post(ajaxurl, {
                action: 'get_workshop_data',
                postid: postid
            }, function(data) {
                var $response = $(data);
                $('.workshopData').html("");
                $('.workshopData').html($response);
            });
        });
        $('.uk-close').click(function() {
            $('.workshopData').html("");
        });
        // $(document).click(function(){
        //     $('.workshopData').html("");
        // });
    }

    function get_course_data() {
        $('.get_course').click(function() {
            var postid = $(this).attr('data-post-id');
            $.post(ajaxurl, {
                action: 'get_course_data',
                postid: postid
            }, function(data) {
                var $response = $(data);
                $('.courseData').html("");
                $('.courseData').html($response);
            });
        });
    }

    function submit_enrolment() {
        $('#enrolment-frm').submit(function(e) {
            
            if($("form")[0].checkValidity()) {
                $.post(ajaxurl, {
                    action: 'send_enrolment_mail',
                    child_name: $('#child_name').val(),
                    child_age: $('#child_age').val(),
                    child_dob: $('#child_dob').val(),
                    parent_name: $('#parent_name').val(),
                    parent_mobile: $('#parent_mobile').val(),
                    parent_email: $('#parent_email').val(),
                    like_to_try: $('#like_to_try').val(),
                    date_of_trial: $('#date_of_trial').val(),
                    time_of_class: $('#time_of_class').val()
                }, function(data) {
                    var $response = $(data);
                    $('.courseData').html("");
                    $('.courseData').html($response);
                    $('.stripe-button-el').click();
                });
            }else{
                $('#enrolment-frm').find( ":invalid" ).first().focus();
            } 
            e.preventDefault();
        });
    }

    function click_pay_term_button() {
        $(document).on('click', '.pay-term-now', function() {
            $('.stripe-button-el').click();
        });
    }

    function submit_workshop_booking() {
        $(document).on('click', '#workshop_booking', function(e) {
            if($("form")[0].checkValidity()) {
                var formData = $('#workshop-frm').serialize();
                // console.log(formData);
                $.post(ajaxurl, {
                    action: 'send_workshop_register_mail',
                    data: formData,
                }, function(data) {
                    // console.log(data);
                    $('#temp').html(data);
                    setTimeout(function() {
                        $('.stripe-button-el').click();
                    }, 1000);
                    // $(document).trigger( "click", '.stripe-button-el' );
                });
                e.preventDefault();
            }else{
                $('#workshop-frm').find( ":invalid" ).first().focus();
            }
            
        });
    }

    function workshop_update_price() {
        // console.log($('input[name=day_booking]:checked').val());
        $('#total_price').val(0);
        var total_price = 0;
        $('.day_booking').on('change', function() {
            total_price = parseFloat($(this).val());
            if($('#late_pickup').is(':checked')){
                total_price += parseFloat($('#late_pickup').val());
            }
            if($('#early_drop').is(':checked')){
                total_price += parseFloat($('#early_drop').val());
            }
            $('#total_price').val(total_price);
        });

        $('#late_pickup').on('change', function() {
            if ($(this).is(':checked')) {
                total_price += parseFloat($(this).val());
            } else {
                total_price -= parseFloat($(this).val());
            }
            $('#total_price').val(total_price);
        });
        $('#early_drop').on('change', function() {
            if ($(this).is(':checked')) {
                total_price += parseFloat($(this).val());
            } else {
                total_price -= parseFloat($(this).val());
            }
            $('#total_price').val(total_price);
        });
    }

    function add_placeholder_in_booking_form() {
        jQuery(".input-field-row_seats #row_seats").attr("placeholder", "Wheelchair access or end of row seats");
    }
    // $(".stripe_checkout_app").load(function() {
    //     $(".stripe_checkout_app").contents().on("click", ".Header-navClose", function() {
    //         alert("hello");
    //     });
    // });
    

    
    var iframe = $('.stripe_checkout_app').contents();

    iframe.find('Header-navClose').click(function(event){
       console.log('work fine');
    });


    workshop_update_price();
    click_pay_term_button();
    submit_enrolment();
    get_workshop_data();
    get_course_data();
    submit_workshop_booking();
    add_placeholder_in_booking_form();
});

