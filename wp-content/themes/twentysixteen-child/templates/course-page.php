<?php
/**
 * Template Name: Course Page
 */
?>
<?php get_header(); ?>

<section id="content">
  <div class="uk-block">
    <div class="uk-container uk-container-center">
      <div class="tabs-navigation">
      	<?php $categories = get_categories();?>
        <ul id="age-groups" class="uk-subnav uk-subnav-pill uk-flex uk-flex-space-around uk-link-reset">
        	<li data-uk-filter="" class="uk-button clear"><a href="">All</a></li>
		<?php $temp_li = array();?>
        	<?php foreach ($categories as $category): ?>
			<?php $category_order = get_field('category_order',$category);
				$temp_li[$category_order] = '<li data-uk-filter="group-'.$category->slug.'" class="uk-button" style="background-color: '.get_field('category_color',$category).' "><a href="">'. $category->name.'</a></li>';
			?>
        	<?php endforeach; ?>
		<?php ksort($temp_li); ?>
		<?php foreach($temp_li as $li):?>
			<?php echo $li; ?>
        	<?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>
  <div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center">
      <div class="tabs-content">
        <h3 class="uk-text-center">Classes</h3>
        <div data-uk-grid="{controls: '#age-groups'}" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}" class="uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-4" style="position: relative; height: auto;">
      	<!-- repeatable-->
          <?php 

           $args = array('post_type' => 'course', 'posts_per_page'=> -1);
		   $query = new WP_Query($args);
		   while ($query->have_posts()) : $query->the_post();

		   $category_detail = get_the_category($query->post->ID); //$post->ID
		$cat_slug = "";		    
		    foreach ($category_detail as $cd) { 
              	$cource_name_html = '<p class="uk-text-bold"><span style="color:'. get_field('category_color',$cd).'"> '. $cd->name .'</span></p>';
						$cat_slug .= "group-".$cd->slug.",";
				    }
           ?>
          <div  data-uk-filter="<?php echo $cat_slug; ?>" class="class uk-margin-large-bottom" data-grid-prepared="true" aria-hidden="false" >
            <div class="uk-panel uk-panel-box uk-text-center" style="min-height: 321px;">
            
            <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($query->post->ID), 'full' ); ?>
              <div class="uk-panel-teaser" style="background: url('<?php echo $backgroundImg[0]; ?>'); height: 250px; background-repeat:no-repeat; background-position: center; background-size: cover;"><!-- <?php the_post_thumbnail(); ?> --></div>
              <h6 class="uk-text-truncate"><?php the_title(); ?></h6>
              <?php echo $cource_name_html; ?>
              <p class="uk-text-clamp"><?php echo get_the_excerpt(); ?></p>
              <a href="#course-ws001" data-post-id="<?php the_ID(); ?>" class="get_course uk-button" data-uk-modal="{center:true}" style="background-color: #1591cb ">Book Now</a>
            </div>
          </div> 
	
          <?php endwhile; ?>
        <!-- repeatable-->
        </div>
      </div>
      <!-- Modal-->        
	<div id="course-ws001" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
	    <div class="uk-modal-dialog uk-modal-dialog-lightbox" style="top: 121px;">
		
		<a class="uk-modal-close uk-close uk-close-alt"></a>
		<div class="courseData">
		    
		</div>
	    </div>
	</div> 
    </div>
  </div>
</section>
<?php get_footer(); ?>