
jQuery(document).ready(function($) {
	doFontCheck($);
	$(document).on('media_button_content_buttons_load', {jq: $}, fontEvent );

}); /* END OF JQUERY */

	//var fonts_loaded = [];

function fontEvent(e)
{
	doFontCheck(e.data.jq);
}

function doFontCheck($)
{
	var modal = document.querySelector('.max-modal');
	var check_fonts = [];

	/* This is overly complicated since this is the only way to pull pseudo-elements via JS */
	if(modal)
	{
		var nodeList = modal.querySelectorAll('.maxbutton');
		for (i = 0; i < nodeList.length; i++)
		{
			var item = nodeList[i];
			var ff = window.getComputedStyle(
			    item, ':after'
			).getPropertyValue('font-family');

			if (ff && ff.length > 0 )
				check_fonts.push(ff);
		}
	}

	$('.mb-text, .mb-text2').each(function () {
		var ff = $(this).css('fontFamily');
		var weight = $(this).css('fontWeight');

		if (typeof $(this).data('nofont') !== 'undefined')
		{
			return;
		}

		if (typeof ff === 'undefined') // can happen in page builders
			return;

		check_fonts.push(ff);
	});

	// check found fonts.
	for (i = 0; i < check_fonts.length; i++)
	{
		var ff = check_fonts[i];
		ff = ff.replace(/\'|\"|/g,''); // Chrome returns font family with ' in it.

		font_found = false;
		for (j=0; j < fonts_loaded.length; j++) // prevent double loading
		{
			if (fonts_loaded[i] == ff)
			{
				font_found = true;
				continue;
			}
		}

		if (! font_found)
		{
			fonts_loaded.push(ff);
	 		mbpro_loadFontFamilyStylesheet(ff);
		}
	}


} // doFontCheck

function mbpro_loadFontFamilyStylesheet(font_family) {
	if (font_family.indexOf(",", font_family) >= 0)
		return; // a non-google font with comma, probably set by theme. This can happen if button doens't have font preference.

	var font_family_url = mbpro_getFontFamilyUrl(font_family);
	if (font_family_url != "") {

		jQuery("head").append("<link rel='stylesheet' type='text/css' href='" + font_family_url + "' />");
	}
}

function mbpro_getFontFamilyUrl(font_family) {
	var system_fonts = ['', 'Arial', 'Courier New', 'Georgia', 'Tahoma', 'Times New Roman', 'Trebuchet MS',
	'Verdana' ];

	if (system_fonts.indexOf(font_family) >= 0)
		return ""; // not further action needed

	var base_url = '//fonts.googleapis.com/css?family=';
	var family = font_family.replace(/\s/g, '+');
	return base_url + family;

}
