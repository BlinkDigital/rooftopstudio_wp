<?php

namespace MaxButtons;
defined('ABSPATH') or die('No direct access permitted');
$blockClass["basic"] = "basicBlockPro";

use \simple_html_dom as simple_html_dom;


class basicBlockPro extends basicBlock
{

  function admin_fields()
  {
      parent::admin_fields();

      $admin = MB()->getClass('admin');

      $icon_url = MB()->get_plugin_url() . 'images/icons/' ;
      // TEXT
      $field_text = new maxField();
      $field_text->label = __('Text 2','maxbuttons');
      $field_text->name = 'text2';
      $field_text->id = 'text2';
      $field_text->value = maxBlocks::getValue('text2') ;
      $field_text->help = __('Shortcode attribute: text2', 'maxbuttons');
      $admin->insertField('button_width', $field_text, 'start', 'end');

      // FONTS
      $fonts = MB()->getClass('admin')->loadFonts();

      $field_font = new maxField('generic');
      $field_font->label = __('Font 2','maxbuttons');
      $field_font->name = 'font2';
      $field_font->id = $field_font->name;
      $field_font->value= maxBlocks::getValue('font2');
      $field_font->content = maxUtils::selectify($field_font->name, $fonts, $field_font->value);
      $admin->insertField('button_width', $field_font, 'start', '');

      // FONT SIZE
      $field_size = new maxField('number');
      $field_size->name = 'font_size2';
      $field_size->id= $field_size->name;
      $field_size->inputclass = 'tiny';
      $field_size->min = 1;
      $field_size->value = maxUtils::strip_px(maxBlocks::getValue('font_size2'));
      $admin->insertField('button_width', $field_size, '', '');

      $fweight = new maxField('checkbox');
      $fweight->icon = 'dashicons-editor-bold';
      $fweight->title = __("Bold",'maxbuttons');
      $fweight->id = 'check_fweight2';
      $fweight->name = 'font_weight2';
      $fweight->value = 'bold';
      $fweight->inputclass = 'check_button icon';
      $fweight->checked = checked( maxBlocks::getValue('font_weight2'), 'bold', false);
      $admin->insertField('button_width', $fweight, 'group_start', '');

      $fstyle = new maxField('checkbox');
      $fstyle->icon = 'dashicons-editor-italic';
      $fstyle->title = __("Italic",'maxbuttons');
      $fstyle->id = 'check_fstyle2';
      $fstyle->name = 'font_style2';
      $fstyle->value = 'italic';
      $fstyle->inputclass = 'check_button icon';
      $fstyle->checked = checked( maxBlocks::getValue('font_style2'), 'italic', false);
      $admin->insertField('button_width',$fstyle, '','');

      $fline = new maxField('checkbox');
      $fline->icon = 'dashicons-editor-underline';
      $fline->title = __("Underline",'maxbuttons');
      $fline->id = 'check_fline2';
      $fline->name = 'text_underline2';
      $fline->value = 'underline';
      $fline->inputclass = 'check_button icon';
      $fline->checked = checked( maxBlocks::getValue('text_underline2'), 'underline', false);
      $admin->insertField('button_width', $fline,'', 'group_end');

      $falign_left = new maxField('radio');
      $falign_left->icon = 'dashicons-editor-alignleft';
      $falign_left->title = __('Align left','maxbuttons');
      $falign_left->id = 'radio_talign_left2';
      $falign_left->name = 'text_align2';
      $falign_left->value = 'left';
      $falign_left->inputclass = 'check_button icon';
      $falign_left->checked = checked ( maxblocks::getValue('text_align2'), 'left', false);
      $admin->insertField('button_width', $falign_left, 'group_start', '');

      $falign_center = new maxField('radio');
      $falign_center->icon = 'dashicons-editor-aligncenter';
      $falign_center->title = __('Align center','maxbuttons');
      $falign_center->id = 'radio_talign_center2';
      $falign_center->name = 'text_align2';
      $falign_center->value = 'center';
      $falign_center->inputclass = 'check_button icon';
      $falign_center->checked = checked( maxblocks::getValue('text_align2'), 'center', false);
      $admin->insertField('button_width', $falign_center, '','');

      $falign_right = new maxField('radio');
      $falign_right->icon = 'dashicons-editor-alignright';
      $falign_right->title = __('Align right','maxbuttons');
      $falign_right->id = 'radio_talign_right2';
      $falign_right->name = 'text_align2';
      $falign_right->value = 'right';
      $falign_right->inputclass = 'check_button icon';
      $falign_right->checked = checked( maxblocks::getValue('text_align2'), 'right', false);
      $admin->insertField('button_width', $falign_right, '',array('group_end','end') );

      $ptop = new maxField('number');
      $ptop->label = __('Padding 2', 'maxbuttons');
      $ptop->id = 'padding_top2';
      $ptop->name = $ptop->id;
      $ptop->inputclass = 'tiny';
      $ptop->before_input = '<img src="' . $icon_url . 'p_top.png" title="' . __("Padding Top","maxbuttons") . '" >';
      $ptop->value = maxUtils::strip_px(maxBlocks::getValue('padding_top2'));
      $admin->insertField('button_width', $ptop, 'start', '');

      $pright = new maxField('number');
      $pright->id = 'padding_right2';
      $pright->name = $pright->id;
      $pright->inputclass = 'tiny';
      $pright->before_input = '<img src="' . $icon_url . 'p_right.png" class="icon padding" title="' . __("Padding Right","maxbuttons") . '" >';
      $pright->value = maxUtils::strip_px(maxBlocks::getValue('padding_right2'));
      $admin->insertField('button_width', $pright, '','');

      $pbottom = new maxField('number');
      $pbottom->id = 'padding_bottom2';
      $pbottom->name = $pbottom->id;
      $pbottom->inputclass = 'tiny';
      $pbottom->before_input = '<img src="' . $icon_url . 'p_bottom.png" class="icon padding" title="' . __("Padding Bottom","maxbuttons") . '" >';
      $pbottom->value = maxUtils::strip_px(maxBlocks::getValue('padding_bottom'));
      $admin->insertField('button_width', $pbottom, '','');

      $pleft = new maxField('number');
      $pleft->id = 'padding_left2';
      $pleft->name = $pleft->id;
      $pleft->inputclass = 'tiny';
      $pleft->before_input = '<img src="' . $icon_url . 'p_left.png" class="icon padding" title="' . __("Padding Left","maxbuttons") . '" >';
      $pleft->value = maxUtils::strip_px(maxBlocks::getValue('padding_left2'));
      $admin->insertField('button_width', $pleft, '', 'end');


      // Add Underline option
      $fline = new maxField('checkbox');
      $fline->icon = 'dashicons-editor-underline';
      $fline->title = __("Underline",'maxbuttons');
      $fline->id = 'check_fline';
      $fline->name = 'text_underline';
      $fline->value = 'underline';
      $fline->inputclass = 'check_button icon';
      $fline->checked = checked( maxBlocks::getValue('text_underline'), 'underline', false);

      $admin->insertField('check_fstyle', $fline,'', '', 'after');

      // Button for FONT MANAGER
      $button = new maxField('generic');
      $button->label = '';
      $button->id = 'manage-fonts';
      $button->name = 'button_fonts';
      $button->content = '<div id="manage-fonts" class="button manage-fonts maxmodal" data-modal="add-fonts" title="' . __("Add additional fonts","maxbuttons-pro") . '" ><i class="dashicons dashicons-editor-spellcheck"></i>' . __('Font Manager', 'maxbuttons-pro') . '</div>';
  // 		$button->output('start','end');
      $admin->insertField('nofollow', $button, 'start', 'end', 'after');

  }
}
