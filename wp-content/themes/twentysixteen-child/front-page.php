<?php
/**
 *Template Name: front page
 */
?>
<?php get_header(); ?>

<!-- section 2  ------------------------------------------------------------------>
<section id="content">
<?php if(get_field('display_section4') == 1): ?> 
  <div class="uk-block">
    <div class="uk-container uk-container-center">
      <div class="uk-block-birds"></div>
      <div class="main-content">
        <div class="uk-grid">
          <div class="uk-width-medium-3-4">
            <h2><?php echo get_field('header_section4');?></h2>
            <?php echo get_field('content_section4');?>
          </div>
          <div class="uk-width-medium-1-4">
            <div class="twiiter-sidebar">
              <a class="twitter-timeline" data-height="500" href="<?php echo get_field('twitter_embeded_url');?>" style="position: static; visibility: visible; display: inline-block; width: 900px; padding: 0px; border: none; max-width: 100%; min-width: 180px; margin-top: 0px; margin-bottom: 0px; height: 500px; min-height: 200px;" >Tweets by RooftopStudios1</a> <!-- <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> --><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- News section ------------------------------------------------------------------>
<div class="uk-block uk-block-gradient uk-contrast">
  <div class="uk-container uk-container-center">
    <div class="news uk-text-center">
      <h3>News</h3>
      <div data-uk-grid-margin="" class="uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-2">
          <?php $args = array('post_type' => 'news','post_per_page' => 2);
             $query = new WP_Query($args); 
             while ($query->have_posts()) : $query->the_post();?>
          <div class="childrens-classes">
            <div class="uk-panel uk-panel-box"><a href="<?php the_permalink(); ?>" class="uk-position-cover"></a>
              <h2><?php the_title(); ?></h2>
              <p><?php echo get_the_excerpt(); ?></p>
              <!-- <div class="uk-panel-teaser"> -->
              <?php // the_post_thumbnail();?>
              <!-- </div> -->
            </div>
          </div>
          <?php endwhile; ?>
          <?php wp_reset_query(); ?>
          </div>
        <div class="uk-block uk-margin-bottom-remove uk-padding-bottom-remove uk-text-center uk-contrast"><a href="<?php echo get_site_url();?>/news" class="uk-button clear-white">VIEW ALL NEWS</a></div>
      </div>
    </div>
  </div>
</div>  

<?php if(get_field('display_section5') == 1): ?> 
  <div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center">
      <div class="believe uk-text-center">
        <h3><?php echo get_field('header_section5');?></h3>
        <p><?php echo get_field('content_section5');?></p>
      </div>
    </div>
  </div>
<?php endif; ?>
  <?php if(get_field('display_section6') == 1): ?>
    <div class="uk-block uk-block-clouds classes-timetables">
      <div class="uk-container uk-container-center uk-text-center">
        <h2 class="uk-contrast"><?php echo get_field('header_section6 ');?></h2>
        <div data-uk-grid-margin="" class="uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-2">
          <?php $b=0; while (has_sub_field('content_section6')): $b++; ?>
          <div class="childrens-classes">
            <div class="uk-panel uk-panel-box"><a href="<?php echo the_sub_field('content_section6_link'); ?>" class="uk-position-cover"></a>
              <div class="uk-panel-teaser"><img src="<?php echo the_sub_field('content_section6_Image'); ?>" alt="teaser"></div>
              <h2><?php echo the_sub_field('content_section6_title'); ?></h2>
              <p><?php echo the_sub_field('content_section6_content'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
          </div>
        <div class="uk-block uk-margin-bottom-remove uk-padding-bottom-remove uk-text-center uk-contrast"><a href="<?php the_field('link_section6');?>" class="uk-button clear-white">VIEW ALL CLASSES AND TIMETABLES</a></div>
      </div>
    </div>
  <?php endif; ?>
  <?php if(get_field('display_section7') == 1): ?>
    <div class="uk-block uk-block-muted">
      <div class="uk-container uk-container-center">
        <div data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}" class="uk-grid uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-4">
          <?php $a=0; while (has_sub_field('tease_section7')): $a++; ?>
          <div class="shows">
            <div class="uk-panel uk-panel-box uk-panel-box-white" style="min-height: 339px;"><a href="<?php echo the_sub_field('tease_section7_page_link'); ?>" class="uk-position-cover"></a>
              <div class="uk-panel-teaser"><img src="<?php echo the_sub_field('tease_section7_image'); ?>" alt="teaser"></div>
              <h5><?php echo the_sub_field('tease_section7_title'); ?></h5>
              <p><?php echo the_sub_field('tease_section7_content'); ?></p>
            </div>
          </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
</section>

<?php if(get_field('display_section2') == 1): ?>
  <!-- section 3  ------------------------------------------------------------------> 
  <section id="testimonials-block">
    <div class="uk-block uk-block-gradient uk-contrast">
      <div class="uk-container uk-container-center">
        <div class="testimonials uk-text-center">
          <h3><?php echo get_field('testimonials_block_header');?></h3>
          <blockquote>
            <?php echo get_field('testimonials_block_blockquote');?>
          </blockquote>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php if(get_field('display_section3') == 1): ?>
  <!-- section 4  ------------------------------------------------------------------>
  
    <?php get_template_part( 'template-parts/content', 'contact' ); ?>
  
<?php endif; ?>

<?php get_footer(); ?>

<script type="text/javascript">
  
  var widgetCSS = "" +
    ".SandboxRoot.env-bp-min .timeline-Tweet{padding:25px;}";
  
function paint(){
  var x = window.matchMedia("(max-width: 767px)");
  var w = document.getElementById("twitter-widget-0").contentDocument;
  var ifrm = document.getElementById('twitter-widget-0');
  if (ifrm !== null){
    var timelineWidget = ifrm.contentWindow.document.getElementsByClassName('timeline-Widget')[0];
    if (timelineWidget !== undefined){
      if (x.matches) {
        timelineWidget.style.height="275px";
        timelineWidget.style.overflow="auto"; 
      }                           
      else {
        timelineWidget.style.height="500px";
        //timelineWidget.style.overflow="auto"; 
      }                                                                                   
      //clearInterval(myInterval);
    } else {

    }
  } else {

  }
  
  var s = document.createElement("style");
  s.innerHTML = widgetCSS;
  s.type = "text/css";
  w.head.appendChild(s);
}
</script>