<?php
/**
 * Template Name: enrolment Page
 */
?>
<?php get_header(); ?>
<style>
	.stripe-button-el{
		display: none !important;
	}
</style>

<section id="content" <?php post_class();?>>
	<div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center">
      <div class="believe uk-text-center">
        <h3><?php the_title();?></h3>
        <?php the_excerpt();?>
      </div>
    </div>
  </div>
  <div class="uk-block">
    <div class="uk-container uk-container-center">
	    <div class="uk-block-birds"></div>
	    <div class="main-content">
			    <div class="uk-width-1-1 uk-form">
			    	
				    
				    <?php 
				    $i_course_fee_structure = 0;
				    while (has_sub_field("course_fee_structure", $_REQUEST['courseId'])) { 
			    		if($i_course_fee_structure == 1){
			    		   	$price = get_sub_field('course_price');
				    		$time = get_sub_field('course_time');
				    	}
				    	$i_course_fee_structure++;
				     } 
				     $price *= 100;
				     echo do_shortcode('[stripe name="Roof Top Studio" description="Enrolment of class" amount='.$price.' currency="GBP"]');
				     ?>
				      <form id="enrolment-frm">
				      <fieldset>
					      <div class="uk-grid">
					      	<div class="uk-width-medium-1-3">
					      		<div class="input">
					      			<label>Child Name
					      			<input type="text" name="child_name" id="child_name" class="input uk-width-1-1" required></label>
					      		</div>
					      	</div>	
					      	<div class="uk-width-medium-1-3">
					      		<div class="input">
					      			<label>Child Age
					      			<input type="number" name="child_age" id="child_age" class="input uk-width-1-1" required></label>
					      		</div>
					      	</div>	
					      	<div class="uk-width-medium-1-3">
					      		<div class="input">
					      			<label>Child DOB
					      			<input type="date" name="child_dob" id="child_dob" class="input uk-width-1-1" required></label>
					      		</div>
					      	</div>	
					      </div>
					      <div class="uk-grid">
					      	<div class="uk-width-medium-1-3">
					      		<div class="input">
					      			<label>Parent/Guardian name
					      			<input type="text" name="parent_name" id="parent_name" class="input uk-width-1-1" required></label>
					      		</div>
					      	</div>	
					      	<div class="uk-width-medium-1-3">
					      		<div class="input">
					      			<label>Parent / Guardian Mob number
					      			<input type="text" name="parent_mobile" id="parent_mobile" class="input uk-width-1-1" required></label>
					      		</div>
					      	</div>	
					      	<div class="uk-width-medium-1-3">
					      		<div class="input email">
					      			<label>Parent email address
					      			<input type="email" name="parent_email" id="parent_email" class="input uk-width-1-1" required></label>
					      		</div>
					      	</div>	
					      </div>
					      	<div class="uk-grid">
					      		<div class="uk-width-medium-1-1">
					      		<div class="input">
						      			<label>Class you would like to book
						      				<input type="text" name="like_to_try"  id="like_to_try" class="input uk-width-1-1" required>	
					      				</label>
					      			</div>	
					      		</div>
					      	</div> 	
					      	<div class="uk-grid">
					      		<div class="uk-width-medium-1-1">
					      		<div class="input">
						      			<label>Day of class
						      				<input type="text" name="date_of_trial"  id="date_of_trial" class="input uk-width-1-1" required>	
					      				</label>
					      			</div>	
					      		</div>
					      	</div> 	
					      	<div class="uk-grid">
					      		<div class="uk-width-medium-1-1">
					      		<div class="input">
						      			<label>Time of class
						      				<input type="text" name="like_to_try"  id="like_to_try" class="input uk-width-1-1" required>	
					      				</label>
					      			</div>	
					      		</div>
					      	</div>
					      	<p>
					      	<button type="submit" class="submit_frm uk-button turquoise uk-width-1-1">Submit</button>
					      	</p>
				      </fieldset>
				      </form>
			    </div>
	    </div>
  </div>
  </div>
  </section>
  <script type="text/javascript">
  // jQuery(function($) {

  // 	$('.submit_frm').click(function(e) {
  // 			e.preventDefault();
  //           $.post(ajaxurl, {
  //               action: 'send_enrolment_mail',
  //               child_name: $('#child_name').val(),
  //               child_age: $('#child_age').val(),
  //               child_dob: $('#child_dob').val(),
  //               parent_name: $('#parent_name').val(),
  //               parent_mobile: $('#parent_mobile').val(),
  //               parent_email: $('#parent_email').val(),
  //               like_to_try: $('#like_to_try').val(),
  //               date_of_trial: $('#date_of_trial').val(),
  //               time_of_class: $('#time_of_class').val()
  //           }, function(data) {
  //               var $response = $(data);
  //               $('.courseData').html("");
  //               $('.courseData').html($response);
  //               $('.stripe-button-el').click();
  //           });
  //       });
  //   });
  </script>

<?php get_footer(); ?>