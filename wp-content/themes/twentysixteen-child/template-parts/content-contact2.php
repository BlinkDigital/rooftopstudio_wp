<?php
/**
 * The template part for displaying content contact
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php $frontpage_id = get_option( 'page_on_front' ); ?>
<section id="short-contact-block">
  <div class="uk-block uk-block-gradient">
    <div class="uk-container uk-container-center uk-contrast uk-text-center">
      <p>
      <span class="uk-text-bold uk-margin-large-left">T: <?php echo get_field('contact_no', $frontpage_id);?></span> 
      <?php echo get_field('address_2', $frontpage_id);?>
      </p>
    </div>
  </div>
</section>